+++
title = "Book notes: Boundaries When To Say Yes, How to Say No. By Henry Cloud and John Townsend"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-08-22
lastmod = 2022-01-11T22:53:37-03:00
tags = ["psychology"]
categories = ["books"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [High-Level Thoughts](#high-level-thoughts)
- [Summary Notes](#summary-notes)
- [Key ideas](#key-ideas)
    - [Quotations](#quotations)
- [TODOs](#todos)
        - [<span class="org-todo todo TODO">TODO</span> Referal link to amazon](#referal-link-to-amazon)

</div>
<!--endtoc-->

title
: Boundaries: when to say yes, how to say no

author
: Dr. Henry Cloud, Dr. John Townsend

format
: book

rating
: 9/10

image
: {{< figure src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348423991l/944267.jpg" >}}

referal
: [Goodreads](https://www.goodreads.com/book/show/530415.The%5FArt%5Fof%5FDoing%5FScience%5Fand%5FEngineering?ac=1&from%5Fsearch=true&qid=JiicJZOxxt&rank=1)


time
:


average speed
:


## High-Level Thoughts {#high-level-thoughts}


## Summary Notes {#summary-notes}


## Key ideas {#key-ideas}

-   You are in pain, and only you have the power to fix it
-   The law of power: "You only have the power to change yourself. You can't
    change another person".
-   You must see yourself as the problem, not the other person
-   To see the other person as the problem to be fixed is to give that person
    power over you
-   We can sustain neither life nor emotional repair without bonding to God and others
-   We can't solve our problem in a vacuum, if we could, we would.
-   Will is only strengthened by relationship. God told Moses to encourage and strengthen Joshua (Deut 3:28)

Self Boundary checklist:

1.  What are the symptoms?
2.  What are the roots?
    1.  Lack of training
    2.  Rewarded destructiveness
    3.  Distorted need
    4.  Fear or felationship
    5.  Unmet emotional hungers
    6.  Imposed decisions
    7.  Covering emotional hurt
3.  What is the boundary conflict?
    in relation to eating, money, time, task completion, the tongue, sexuality or
    alcohol/substance abuse among others
4.  Who needs to take ownership?
5.  What relationships do you need?
6.  How do I begin?
    -   Address the real need
    -   Allow yourself to fail
    -   Listen to feedback from others
    -   Welcome consequences as a teacher
    -   Surround yourself with people who are loving and supportive


### Quotations {#quotations}

-   "You only have the power to change yourself. You can't change another person".
-   "Our boundary conflicts may not be all our fault. They are, however, our responsibility"
-   "Trying to solve a problem by only dealing with the symptoms, generally leads to more symptoms"


## TODOs {#todos}


#### <span class="org-todo todo TODO">TODO</span> Referal link to amazon {#referal-link-to-amazon}
