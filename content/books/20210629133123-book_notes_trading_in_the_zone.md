+++
title = "Book notes: Trading in the zone"
author = ["Albert De La Fuente Vigliotti"]
date = 2021-06-29
lastmod = 2022-01-11T23:02:05-03:00
tags = ["books", "trading"]
categories = ["books"]
draft = false
+++

I very much enjoyed this book. Awesome insights, but there is a bad news...
Trading is too much correlated with self confidence and self mastery. I have
personal issues with that at this time, so it is something I really need to
improve. For that very same reason I will continue on my previous research on
algorithmic trading for the time being.


## Objectives {#objectives}

-   To prove that the solution is not better analysis
-   Attitude and state of mind = results
-   Provide the right attitudes = think in probabilities
-   Address conflicts in the way of thinking
-   Integrate this way of thinking in a functional level


## I am a consistent winner because: (7 must have to be a consistent winner) {#i-am-a-consistent-winner-because--7-must-have-to-be-a-consistent-winner}

-   [ ] I objectively identify my edges
-   [ ] I pre-define the risk of every trade
-   [ ] I completely accept the risk, or let go the trade
-   [ ] I act on my edges without hesitation
-   [ ] I pay myself as the market makes money available to me
-   [ ] I continue to monitor my susceptibility to make errors (being aware)
-   [ ] I never violate these principles
