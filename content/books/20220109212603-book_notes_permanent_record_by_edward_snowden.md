+++
title = "Book notes: Permanent record by Edward Snowden"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-03-08
lastmod = 2023-08-15T00:29:34-03:00
tags = ["privacy", "surveillance"]
categories = ["books"]
draft = false
+++

title
: Permanent Record

author
: Edward Snowden

links
: [Goodreads](https://www.goodreads.com/book/show/530415.The_Art_of_Doing_Science_and_Engineering?ac=1&from_search=true&qid=JiicJZOxxt&rank=1)

personal rating
: 9/10


## Personal comments {#personal-comments}

I found the book a delight. It reminded me my own experience growing since we
have so many similarities. I also dismantled things, I also lived up internet on
the 90's on bulletin boards and cracking groups.


## Actionable tasks {#actionable-tasks}


### <span class="org-todo todo TODO">TODO</span> How to establish a secured connection over satellite if possible {#how-to-establish-a-secured-connection-over-satellite-if-possible}


### <span class="org-todo todo TODO">TODO</span> How to create my own TOR relay to mix in with other's traffic {#how-to-create-my-own-tor-relay-to-mix-in-with-other-s-traffic}


### <span class="org-todo todo TODO">TODO</span> How to use pihole {#how-to-use-pihole}
