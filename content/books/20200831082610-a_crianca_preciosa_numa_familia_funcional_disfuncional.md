+++
title = "Book notes: A crianca preciosa numa familia funcional/disfuncional"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-08-31
lastmod = 2022-01-11T22:52:32-03:00
tags = ["portuguese", "psychology"]
categories = ["books"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [A crianca preciosa numa familia funcional](#a-crianca-preciosa-numa-familia-funcional)
    - [Como uma familia funcional da apoio ao valor da crianca](#como-uma-familia-funcional-da-apoio-ao-valor-da-crianca)
    - [A crianca vulneravel](#a-crianca-vulneravel)
        - [Como uma familia funcional protege a crianca vulneravel](#como-uma-familia-funcional-protege-a-crianca-vulneravel)
    - [A crianca imperfeita](#a-crianca-imperfeita)
        - [De que modo uma familia funcional da a poio a imperfeicao da crianca](#de-que-modo-uma-familia-funcional-da-a-poio-a-imperfeicao-da-crianca)
    - [A crianca dependente](#a-crianca-dependente)
        - [De que modo uma familia funcional satisfaz e supre os desejos e as necessidades de uma crianca](#de-que-modo-uma-familia-funcional-satisfaz-e-supre-os-desejos-e-as-necessidades-de-uma-crianca)
    - [A crianca imatura](#a-crianca-imatura)
        - [Como uma familia funcional ampara a imaturidade da ciranca](#como-uma-familia-funcional-ampara-a-imaturidade-da-ciranca)
- [A crianca preciosa numa familia disfuncional](#a-crianca-preciosa-numa-familia-disfuncional)
    - [Ligacao entre as carateristicas da crianca e codependencia](#ligacao-entre-as-carateristicas-da-crianca-e-codependencia)
    - [Valor da crianca numa familia disfuncional](#valor-da-crianca-numa-familia-disfuncional)
    - [Carateristicas do adulto codependente](#carateristicas-do-adulto-codependente)
    - [Vulnerabilidade da crianca numa familia disfuncional](#vulnerabilidade-da-crianca-numa-familia-disfuncional)

</div>
<!--endtoc-->

I am all thumbs, there might be typos here. Spelling a document takes too much
time and since probably no one else but me reads this anyway, I will save some
time by not spelling it on purpose. If this bothers you, go read the NY Times
instead...


## A crianca preciosa numa familia funcional {#a-crianca-preciosa-numa-familia-funcional}

-   Atributos vs carateristicas ao crescer que devem ser desenvolvidas na infancia
    -   Preciosidade: auto-estima
    -   Vulnerabilidade: limites funcionais
    -   Imperfeicao: Responsavel pelos atos
    -   Dependente: Interdependencia capaz de satisfazer desejos e necessidades
    -   Imatura: Amadurecimento de acordo com a faixa etaria
-   Todos os membros da familia possuem o mesmo valor


### Como uma familia funcional da apoio ao valor da crianca {#como-uma-familia-funcional-da-apoio-ao-valor-da-crianca}

-   Compartilhar o poder com a crianca (voce pode ir sozinho ou ter a minha ajuda)
-   Evitar posturas disfuncionais (dizer nao a crianca e sim para o outro, ou seja voce so pode fazer o que eu quero que faca)

Passos para dar uma resposta certa:

-   Admitir ter ouvido
-   Falar a regra e o motivo da mesma
-   Dizer como a crianca vai ser ajudada a cumprir a regra
-   Realmente e feito aquilo que foi dito que seria feito de forma firme
-   Consequencias caso haja desobediencia

-   Tambem e importante ensinar a crianca que tem escolhas
-   Muitos codependentes perderam o sentido de escolher


### A crianca vulneravel {#a-crianca-vulneravel}

-   Respeitar o direito da crianca a seus proprios pensamentos, sentimentos e comportamentos enquanto e educada


#### Como uma familia funcional protege a crianca vulneravel {#como-uma-familia-funcional-protege-a-crianca-vulneravel}

-   Pais sao adultos funcionais
-   Colocam limites
-   Nao atacam a crianca e tem conduta adequada
-   Nao super proteger nem sub proteger


### A crianca imperfeita {#a-crianca-imperfeita}

-   Criancas cometem erros o tempo todo
-   Numa familia funcional se tem a consciencia que TODOS da familia SAO imperfeitos


#### De que modo uma familia funcional da a poio a imperfeicao da crianca {#de-que-modo-uma-familia-funcional-da-a-poio-a-imperfeicao-da-crianca}

-   Pedir desculpas e se redimir diante da crianca de tempos em tempos
-   Aceitar que a crianca e imperfeita
-   Evitar que uma crianca se desculpe quando nao deve
-   Dar tempo necessario para refletir dos erros e pedir perdao
-   Obrigacao de arrumar, limpar, substituir o quebrado, sujado ou perdido
-   Quando ninguem da familia e um "deus" tem espaco para um Deus


### A crianca dependente {#a-crianca-dependente}

-   Carinho emocional: dividido em dois, quem elas sao e como fazer as coisas
-   A crianca se torna quem os pais dizem que elas sao
-   A crianca vai desenvolvendo um senso de identidade (tambem influenciado nas atitudes dos pais)
-   Passar valores
-   Instrucao sexual
-   Instrucao financeira: comprar, reuniao planejamento de ferias baseado em orcamento


#### De que modo uma familia funcional satisfaz e supre os desejos e as necessidades de uma crianca {#de-que-modo-uma-familia-funcional-satisfaz-e-supre-os-desejos-e-as-necessidades-de-uma-crianca}

-   Ser receptivo e antecipar as necessidades (quando e pequeno)
-   Identificar os desejos e necessidades
-   Desenvolver a interdependencia (Recorrer as pessoas adequadas para suprir os desejos e necessidades que nao podem ser auto-satisfeitos, exemplo: um abraco)


### A crianca imatura {#a-crianca-imatura}

-   Criancas sao imaturas, ponto.


#### Como uma familia funcional ampara a imaturidade da ciranca {#como-uma-familia-funcional-ampara-a-imaturidade-da-ciranca}

-   Saber o que esperar de cada faixa etaria da crianca
-   Nao exigir comportamentos fora da faixa etaria
-   Raiva e maus comportamentos nao devem ser menosprezados mas conversados com "o que esta acontecendo com voce?"


## A crianca preciosa numa familia disfuncional {#a-crianca-preciosa-numa-familia-disfuncional}

Os tres atributos da crianca

-   Egocentrismo
-   Energia ilimitada
-   Adaptabilidade

Esses atributos sao usados contra as criancas em familias disfuncionais. Ex:
Egocentrismo, os pais querem que estejam centrados neles!

A cura da codependencia e um processo bastante semelhante ao de crescimento:
(aprender a fazer as coisas que nao nos ensinaram)

-   Ter uma autoestima adequada por nos mesmos
-   Cuidar dos nossos desejos e necessidades
-   Vivenciar a realidade


### Ligacao entre as carateristicas da crianca e codependencia {#ligacao-entre-as-carateristicas-da-crianca-e-codependencia}

-   As criancas acreditam que os pais nao erram
-   Quase todo pai ofende ou maltrata a crianca pela imperfeicao, dependencia e imaturidade
-   As criancas acabam perdendo sua nocao de valor


### Valor da crianca numa familia disfuncional {#valor-da-crianca-numa-familia-disfuncional}

-   A mensagem passada e: ha algo errado em vc, ajeite-se
-   O fato de vc precisar que eu lhe faca tantas coisas significa que eu sou melhor
-   Os pais fazem negar os desejos e a dependencia dos filhos repetidamente
-   Forcam a crianca a agir de forma mais velha ou mais nova

Efeito da educacao disfuncional nas carateristicas naturais da crianca (sintomas de codependencia)

-   Preciosa: Dificuldade em vivenciar niveis apropriados de auto-estima
-   Vulneravel: Dificuldade em apropriar-se dos limites funcionais
-   Imperfeita: Dificuldade em aceitar e expressar a propria realidade e imperfeicao
-   Dependente com necessidades e desejos: Dificuldade em tomar conta dos proprias necessidades e desejos
-   Imatura: Dificuldade em vivenciar e expressar moderadamente a propria realidade

-   As criancas poderam adquirir estima aleia (notas escolares, vestimenta, amigos com quem saem, etc)
-   Tendencia de esforcar-se muito para agradar aos outros por nao saber como gostar de si mesmo


### Carateristicas do adulto codependente {#carateristicas-do-adulto-codependente}

-   Problema principal: falta de consciencia do valor pessoal
-   Efeitos: baixa autoestima ou arrogancia (ou ate alternancia de ambas)


### Vulnerabilidade da crianca numa familia disfuncional {#vulnerabilidade-da-crianca-numa-familia-disfuncional}

-
