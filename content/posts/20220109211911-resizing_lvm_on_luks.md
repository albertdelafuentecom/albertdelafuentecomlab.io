+++
title = "Resizing lvm on luks"
author = ["Albert De La Fuente Vigliotti"]
date = 2015-11-01
lastmod = 2023-08-13T11:23:43-03:00
tags = ["archlinux", "system", "luks", "lvm"]
categories = ["linux"]
draft = false
image = "/images/fm_recover_grub.png"
+++

## How to manually boot/recover a system with LUKS and LVM {#how-to-manually-boot-recover-a-system-with-luks-and-lvm}

I have been using LVM on top of LUKS for more than 2 years without problems. However since I haven't had the chance to troubleshoot these technologies in the past I was quite concerned of what would happen when I would have problems. Today my main computer didn't boot up and I had to fix it manually. This post shows the steps I performed to recover my system.


### Intro {#intro}

```sh
cryptsetup luksOpen /dev/sda2 cryptlvm

e2fsck -f /dev/mapper/storage-home

resize2fs -p /dev/mapper/storage-home 200g

e2fsck -f /dev/mapper/storage-home

(lvdisplay)
lvreduce -L -15.68G /dev/storage/home
(lvdisplay)

[lvresize -l +100%FREE /dev/storate/root]

e2fsck -f /dev/mapper/storage-root

resize2fs /dev/mapper/storage-root

cryptsetup luksClose /dev/sda2 cryptlvm


### NOT NEEDED

(pvdisplay)
pvresize --sephysicalvolumesize 218G /dev/mapper/cryptlvm

/dev/mapper/cryptlvm is active and is in use
  type:    LUKS1
  cipher:  aes-xts-plain64
  keysize: 256 bits
  device:  /dev/sda2
  offset:  4096 sectors
  size:    487981391 sectors
  mode:    read/write

NEW_SECTORS = EXISTING_SECTORS * NEW_SIZE_IN_GB / EXISTING_SIZE_IN_GB
487981391 * 200.7 / 232.7   = 420876085
(487981391 - 15*1024*1024*2 = 456524111)

cryptsetup -b 420876085 resize cryptlvm

(cryptsetup status cryptlvm)

parted /dev/sda

resize
```
