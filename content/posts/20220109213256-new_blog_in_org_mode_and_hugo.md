+++
title = "New blog in org-mode and Hugo"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-08-07
lastmod = 2023-08-13T11:17:28-03:00
slug = "sandbox"
tags = ["org-mode", "hugo"]
categories = ["emacs"]
draft = false
image = "/images/fm_hugo_org_mode_blog.png"
+++

Okay this is like my fifth attempt to blog and so far, unsuccessfully.

My first couple of blogs were on blogspot around 2005 or so, later I switched to
dokuwiki, which is awesome but it didn't integrated well on my workflow. So I
tried some extensions to try to write directly in markdown but it didn't work
well for me. Then I moved to Hyde, which a Python static website generator. I
invested quite some time customizing everything to my needs and while it was
functional, I had to write in markdown (which I don't like...). I tried to write
in RST at that time without success.

Then I moved from Vim to [Emacs](https://gnu.org/software/emacs/) and discovered [org-mode](http://orgmode.org/)... And yeah, everything
changed... I love Emacs+Org-Mode and I used quite a lot (and I still use Vim
occasionally).

I wanted to write directly in Org-Mode for my new blog to see if content flows
this time (hopefully) and I can achieve the integration I was looking for all
these years.

The version of this blog uses plain Org-Mode and Hugo with ox-hugo as a middle
helper. So far, so good, I just migrated my old posts which are few in number.

What I am looking forward is to create a mix of a Blog and a digital garden for
my notes (also known as second brain or exobrain).

The rest of the file is actually a sandbox of general Org-Mode formatting to
check out how it renders on Hugo itself.


## This is H1 {#this-is-h1}

There is paragraph under h1


### H2 {#h2}

<!--list-separator-->

-  H3

    Something

<!--list-separator-->

-  H3 with a tag <span class="tag"><span class="sometag">sometag</span></span>


## Some basic test {#some-basic-test}

This is **bold**, _italic_, `code`, `verbatim` and ~~strike~~ text.

-   However **_bold and italic_** doesn't play well when used together like in markdown.
-   However _**bold and italic**_ doesn't play well when used together like in markdown.


## List {#list}

-   Bullet
-   Another bullet
    -   child
        -   deep


### Other style {#other-style}

-   Bullet
-   Another bullet
    -   child
        -   deep


### Other style {#other-style}

1.  Bullet
2.  Another bullet
    1.  child
        1.  deep

Style `*` isn't supported.


## Links {#links}

[link to org mode homepage](http://orgmode.org/)


## Check List <code>[1/3]</code> <code>[33%]</code> {#check-list}

-   [ ] Item
-   [ ] Item
-   [X] Checked item

Heading and has special class however `<ul>` and `<li>` are plain.


## Task List {#task-list}


### <span class="org-todo todo TODO">TODO</span> some to-do {#some-to-do}


### <span class="org-todo done DONE">DONE</span> done to-do {#done-to-do}

Items are added with special class.


## Tables {#tables}

| number | description        |
|--------|--------------------|
| 1      | looooong long name |
| 5      | name               |

`<tr>` has `even` and `odd` classes.


## Source Code {#source-code}

**Emacs Lisp:**

```emacs-lisp
(defun negate (x)
    "Negate the value of x."
    (- x))
```

```emacs-lisp
(print
    (negate 10))
```

There are interesting classes like `sourceCode` and `example`.
Also there html5 attributes prefixed with `rundoc-`.

**Haskell:**

```haskell
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)
```


## LaTeX {#latex}

-   **Characters:** &alpha; &beta; &rarr; &uarr; \or \and \implies &pi; &infin;
-   **Inline Math:** \\(f(x) = x^2\\)
-   **More complex:** \\(\frac{x^2}{2}\\)

LaTeX characters are wrapped in `<em>` and Math inside `<span class="math inline">`.


### \mathscr{Hello!} {#mathscr-hello}

\begin{align\*}
  8 \* 3 &= 8 + 8 \\\\
        &= 24
\end{align\*}

**NOTE:** _There is standard LaTeX embeded above which is skipped during compilation to HTML._

**This is using** [MathJax](https://www.mathjax.org/)

\\[\sum\_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}\\]


## Deadline {#deadline}


## Tagged <span class="tag"><span class="tag">tag</span></span> {#tagged}

Tags are not visible in render


## Block Quote {#block-quote}

> Org mode is amazing. So is Hakyll &amp; Pandoc.


## Image {#image}

<http://media.riffsy.com/images/f8534774b678ad1932b379a03460680b/raw>

Images has to have extension like:

then it can be loaded even from other origin..

{{< figure src="https://media.tenor.com/Vkid3xkvTAMAAAAd/dog-in-space-dog.gif" >}}


## Description List {#description-list}

Frodo
: The hobbit ringbearer

Aragorn
: The human ranger, true kind of Gondor

Gandalf
: The Grey Wizard

creddits to [nihilmancer](https://www.reddit.com/user/nihilmancer)

[^fn:1]: The link is: <http://orgmode.org>