+++
title = "Software patents"
author = ["Albert De La Fuente Vigliotti"]
date = 2013-01-08
lastmod = 2023-08-13T11:38:29-03:00
tags = ["software", "floss", "patents"]
categories = ["development"]
draft = false
image = "/images/fm_patents.jpg"
+++

Recently I've been to a talk given by Richard Stallman about how software
patents are useless for the software industry. Since software is applied
mathematics and, it generally needs to combine several ideas to make a new one,
by doing that you could easily infringe a software patent by using one (or more)
of the patented ideas.

{{< figure src="/home/av/repos/websites/albertdelafuente.com-hugo/static/images/fm_patents.jpg" >}}

He commented that patents are like walking in a mine field, but worst because a
mine ones it blows up it doesn't blows again, patents keeps blowing on and on.
He later highlighted some different scenarios.


## A patent for your idea not to be "stolen"\*\*. This is the worst case scenario {#a-patent-for-your-idea-not-to-be-stolen-dot-this-is-the-worst-case-scenario}

You might think that you had a great idea and you want to patent it so no one
will stole your idea. Later a big company comes in and develops something around
your idea. Then something similar will happen...

&gt; (You) You go there and say: Hey this idea is mine, and I have the patent so you cannot use it!

&gt;&gt; (Big company) Ohh, it is a shame... But let me remind you that within your idea you used some ideas similar to these three patents of us...

&gt; (oh oh)

&gt;&gt; ...and we can find other patents (among our thousands) of us, you might violated with your idea as well...

&gt; ehmmm

&gt;&gt; ...so in order to no one get hurts, lets make a deal: You give us the right to use your patent and we wont sue you

&gt; Well... You evaluate (in silence) and remember that they have some good lawyers and a lot of money as well... And you end by saying: Okay you can use it

&gt;&gt; Great. Thanks =D

**Conclusion: The big company wins.**

{{< figure src="/home/av/repos/websites/albertdelafuente.com-hugo/static/mobile-patent-suits.png" >}}


## A patent to protect yourself from others attacking you {#a-patent-to-protect-yourself-from-others-attacking-you}

The truth is that it won't protect you unless you find another infringement on the other side that is covered by your patent. A minor company wont sue you anyway since they don't have the resources, and a big one will have very good lawyers to do so and to also defend themselves, so it's a endless legal battle and unnecessary waste of money.

He also pointed that in fact a patent could be also be harmful if you try to defend yourself you don't know how to use it properly, or something is wrong within the patent.


## Conclusion {#conclusion}

A never ending legal story if someones attacks you, or in the best scenario
nobody will attack you so you won't really need the patent in first place.

Later he also made a symbolism about software and music which personally I found
very interesting. "Imagine that when Beethoven was alive the whole patent idea
arose and somebody patented parts of musical pieces. Then some beautiful
compositions would never been made. It's easier to compose for the beauty of
doing so than composing with the worry of not getting sued".

The whole speech makes a lot of sense while patents in software don't.
