+++
title = "How to recover a GRUB bootloader on a filesystem with LUKS and LVM"
author = ["Albert De La Fuente Vigliotti"]
date = 2021-08-04
lastmod = 2023-08-13T11:19:32-03:00
tags = ["archlinux", "system", "grub", "luks", "lvm"]
categories = ["linux"]
draft = false
image = "/images/fm_recover_grub.png"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [(OPTIONAL) Create a boot flash disk using android](#optional--create-a-boot-flash-disk-using-android)
        - [EtchDroid USB writer | F-Droid - Free and Open Source Android App Repository](#etchdroid-usb-writer-f-droid-free-and-open-source-android-app-repository)
- [Mounting and chrooting the system](#mounting-and-chrooting-the-system)
- [Edit the /etc/default/grub file](#edit-the-etc-default-grub-file)
- [Config and install GRUB and create the ramdisk](#config-and-install-grub-and-create-the-ramdisk)
- [References](#references)
    - [[SOLVED] Grub with lvm on LUKS -&gt; rescue mode. Incomplete grub.cfg? / Installation / Arch Linux Forums](#solved-grub-with-lvm-on-luks-rescue-mode-dot-incomplete-grub-dot-cfg-installation-arch-linux-forums)

</div>
<!--endtoc-->

Recently an upgrade on my notebook went bad and broke something. I wasn't sure
what went wrong but since I had just one system at that time, I had to figure
out how to fix it with the help of my phone. Basically what I did is install
EtchDroid to be able to download an iso and flash it to a flash drive on my
phone directly. Later I could boot form that flash drive, mount the LUKS/LVM
partitions and fix the bootloader. Here is a short how to.


## (OPTIONAL) Create a boot flash disk using android {#optional--create-a-boot-flash-disk-using-android}


#### [EtchDroid USB writer | F-Droid - Free and Open Source Android App Repository](https://f-droid.org/en/packages/eu.depau.etchdroid/) {#etchdroid-usb-writer-f-droid-free-and-open-source-android-app-repository}

-   Source: <https://f-droid.org/en/packages/eu.depau.etchdroid/>
-   Title: EtchDroid USB writer | F-Droid - Free and Open Source Android App Repository
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2021-08-05 Thu]</span></span>


## Mounting and chrooting the system {#mounting-and-chrooting-the-system}

\#+begin_src sh

cryptsetup luksOpen /dev/disk/by-partlabel/cryptlvm lvm

mount /dev/storage/root /mnt
mount /dev/storage/home /mnt/home

mount /dev/sda1 /mnt/boot

cd _mnt
mount -t proc /proc proc_
\#mount -t sysfs _sys sys_
\#mount -o bind _dev dev_
mount -o bind _run run_

arch-chroot /mnt

  dhcpcd eth0
\#+end_src sh


## Edit the /etc/default/grub file {#edit-the-etc-default-grub-file}

\#+begin_src sh

  GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:lvm"
  GRUB_PRELOAD_MODULES="part_gpt part_msdos cryptodisk luks"
\#+end_src sh


## Config and install GRUB and create the ramdisk {#config-and-install-grub-and-create-the-ramdisk}

\#+begin_src sh

grub-mkconfig &gt; /boot/grub/grub.cfg
grub-install --efi-directory=/boot --target=x86_64-efi /dev/sda

  mkinitcpio -p linux
\#+end_src sh


## References {#references}


### [[SOLVED] Grub with lvm on LUKS -&gt; rescue mode. Incomplete grub.cfg? / Installation / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=246628) {#solved-grub-with-lvm-on-luks-rescue-mode-dot-incomplete-grub-dot-cfg-installation-arch-linux-forums}

-   Source: <https://bbs.archlinux.org/viewtopic.php?id=246628>
-   Title: [SOLVED] Grub with lvm on LUKS -&gt; rescue mode. Incomplete grub.cfg? / Installation / Arch Linux Forums
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2021-08-04 Wed]</span></span>
