+++
title = "Learning hebrew like a geek"
author = ["Albert De La Fuente Vigliotti"]
date = 2022-06-01
lastmod = 2023-08-13T11:28:22-03:00
slug = "lhlg"
tags = ["hebrew", "python", "Aleppo", "codex"]
categories = ["scriptures", "development"]
draft = false
image = "/images/fm_lhlg.png"
+++

The other day I was thinking about some practical way of learning hebrew,
something dynamic that tickles my scientist curiosity. And I thought, wouldn't
it be cool if I could parse one of the hebrew codexes and get the top 20 most
frequent words of a chapter or a book? Well this is an attempt to answer these
type of questions.


## How? {#how}

So I rolled my sleeves and I thought: I will google the Aleppo codex in plain
text so I can download it and later use it for analysis. The problem is, I
couldn't find it...

The next challenge then is, how can I produce a plain text version of the Aleppo
codex? I thought about parsing a mysword module, which are in reality a sqlite
database so that should be easy... but then when I was doing something totally
unrelated (probably around the bees or my first batch of mead) it hit me... I
have several bibles from the Sword project already installed on my notebook and
is all OSS, so... there is probably a python wrapper around diatheke or
similar... and sure it is =)

If you want the technicalities, go read my note on [How to parse the Aleppo codex
and analyze its content in python]({{< relref "20220601221125-how_to_parse_the_aleppo_codex_and_analyze_its_content_in_python.md" >}}).

But if you are not a geekie human, you probably are interested only in the
results rather than the bits and bytes. This is why I splitted the whole thing
in two notes, one more IT related and one more Hebrew related. So here we go,
brace yourselves this is probably going to be long.

I am not sure on how to "slice" the codex so the chunks makes sense for an
analysis, for instance: per chapter? Per book? Per "stories"? Or even other
slicing like Torah, Neviim and Ketuvim. Since it is not clear I will start
slicing by books at first.


## Assumptions and limitations {#assumptions-and-limitations}

Hebrew has some peculiarities one of them being the vowel pointing. This brings
a ton of challenges. For the sake of simplicity I had to stick to a codex that
does not include niqqud whatsoever. I am not sure if this is a good approach or
not because two different words without niqqud can render the same writing yet
have totally different meaning.

Another problem is that there are words that are trivially known like לא (h2834)
or אל (h3882); or words that are not translated yet used very much like את
(h7073). I have implemented a filter to have the possibility to skip these
words. Not because they are not relevant, the Aleph-Tav has a ton of secrets and
importance. But there are mixed together some irrelevant tokens considered as
words, like opening and closing brackets and parenthesis or similar. So I
implemented a list to ignore these words that are rather known or really
irrelevant.


## Interesting findings {#interesting-findings}

It was rather interesting to me how fast the repetitions decline on unfiltered
words. For instance the most used word is the  את (h854) with 7073 occurrences.
Yet 30 words later in a row, there is a 90% decline in occurrences, משה (h) with
704 occurrences.


## Experiments organized by sections {#experiments-organized-by-sections}


### Full Tanakh - top 20 words - unfiltered {#full-tanakh-top-20-words-unfiltered}

{{< figure src="/ox-hugo/lhlg_tanakh_nofilter.png" >}}

| Word  | Count | Strong                                                                         |         |
|-------|-------|--------------------------------------------------------------------------------|---------|
| את    | 7073  | <a href="https://www.blueletterbible.org/lexicon/h854/esv/wlc/0-1/">h854</a>   |         |
| יהוה  | 5611  | <a href="https://www.blueletterbible.org/lexicon/h3068/esv/wlc/0-1/">h3068</a> |         |
| אשר   | 4629  | <a href="https://www.blueletterbible.org/lexicon/h834/esv/wlc/0-1/">h834</a>   |         |
| אל    | 3882  | <a href="https://www.blueletterbible.org/lexicon/h410/esv/wlc/0-1/">h410</a>   |         |
| כי    | 3553  | <a href="https://www.blueletterbible.org/lexicon/h3588/esv/wlc/0-1/">h3588</a> |         |
| על    | 3140  | <a href="https://www.blueletterbible.org/lexicon/h5921/esv/wlc/0-1/">h5921</a> |         |
| לא    | 2834  | <a href="https://www.blueletterbible.org/lexicon/h3809/esv/wlc/0-1/">h3809</a> |         |
| כל    | 2757  | <a href="https://www.blueletterbible.org/lexicon/h5921/esv/wlc/0-1/">h5921</a> |         |
| ואת   | 2190  | <a href="https://www.blueletterbible.org/lexicon/h854/esv/wlc/0-1/">h854</a>   |         |
| ישראל | 2085  | <a href="https://www.blueletterbible.org/lexicon/h3479/esv/wlc/0-1/">h3479</a> |         |
| ויאמר | 2043  | <a href="https://www.blueletterbible.org/lexicon/h559/esv/wlc/0-1/">h559</a>   | Related |
| בני   | 1650  | <a href="https://www.blueletterbible.org/lexicon/h1123/esv/wlc/0-1/">h1123</a> |         |
| בן    | 1607  | <a href="https://www.blueletterbible.org/lexicon/h1121/esv/wlc/0-1/">h1121</a> |         |
| ולא   | 1447  | <a href="https://www.blueletterbible.org/lexicon/h3809/esv/wlc/0-1/">h3809</a> | Related |


### Full Tanakh - Top 50 words - unfiltered {#full-tanakh-top-50-words-unfiltered}

{{< figure src="/ox-hugo/lhlg_tanakh_top50.png" >}}

The words from above as the top 20, plus the following words

| Word  | Count | Strong                                                                                                                                                    |         |
|-------|-------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|---------|
| לו    | 1045  | <a href="https://www.blueletterbible.org/lexicon/h3863/esv/wlc/0-1/">h3863</a>                                                                            |         |
| איש   | 1027  | <a href="https://www.blueletterbible.org/lexicon/h376/esv/wlc/0-1/">h376</a>                                                                              |         |
| המלך  | 1014  | <a href="https://www.blueletterbible.org/lexicon/h4428/esv/wlc/0-1/">h4428</a>                                                                            |         |
| בית   | 1003  | <a href="https://www.blueletterbible.org/lexicon/h1004/esv/wlc/0-1/">h1004</a>                                                                            |         |
| מלך   | 1000  | <a href="https://www.blueletterbible.org/lexicon/h4427/esv/wlc/0-1/">h4427</a>                                                                            |         |
| הוא   | 910   | <a href="https://www.blueletterbible.org/lexicon/h1931/esv/wlc/0-1/">h1931</a>                                                                            |         |
| עד    | 904   | <a href="https://www.blueletterbible.org/lexicon/h5704/esv/wlc/0-1/">h5704</a>                                                                            |         |
| לאמר  | 897   | <a href="https://www.blueletterbible.org/lexicon/h559/esv/wlc/0-1/">h559</a> / <a href="https://www.blueletterbible.org/lexicon/564/esv/wlc/0-1/">564</a> |         |
| לך    | 871   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a>                                                                                    |         |
| הארץ  | 856   | <a href="https://www.blueletterbible.org/lexicon/h776/esv/wlc/0-1/">h776</a>                                                                              |         |
| ויהי  | 808   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a>                                                                                    |         |
| אמר   | 797   | <a href="https://www.blueletterbible.org/lexicon/h559/esv/wlc/0-1/">h559</a>                                                                              |         |
| דבר   | 787   | <a href="https://www.blueletterbible.org/lexicon/h1697/esv/wlc/0-1/">h1697</a>                                                                            |         |
| העם   | 724   | <a href="https://www.blueletterbible.org/lexicon/h5971/esv/wlc/0-1/">h5971</a>                                                                            | Related |
| וכל   | 712   | <a href="https://www.blueletterbible.org/lexicon/h3606/esv/wlc/0-1/">h3606</a>                                                                            | Related |
| משה   | 704   | <a href="https://www.blueletterbible.org/lexicon/h4872/esv/wlc/0-1/">h4872</a>                                                                            |         |
| שם    | 681   | <a href="https://www.blueletterbible.org/lexicon/h8043/esv/wlc/0-1/">h8043</a>                                                                            |         |
| מן    | 661   | <a href="https://www.blueletterbible.org/lexicon/h4478/esv/wlc/0-1/">h4478</a>                                                                            |         |
| לי    | 660   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a>                                                                                    |         |
| הזה   | 650   | <a href="https://www.blueletterbible.org/lexicon/h1957/esv/wlc/0-1/">h1957</a>                                                                            |         |
| אני   | 635   | <a href="https://www.blueletterbible.org/lexicon/h589/esv/wlc/0-1/">h589</a>                                                                              |         |
| יהודה | 632   | <a href="https://www.blueletterbible.org/lexicon/h3063/esv/wlc/0-1/">h3063</a>                                                                            |         |
| לפני  | 615   | <a href="https://www.blueletterbible.org/lexicon/h3942/esv/wlc/0-1/">h3942</a>                                                                            |         |
| להם   | 607   | <a href="https://www.blueletterbible.org/lexicon/h3859/esv/wlc/0-1/">h3859</a>                                                                            |         |
| אם    | 607   | <a href="https://www.blueletterbible.org/lexicon/h518/esv/wlc/0-1/">h518</a>                                                                              |         |
| אלהים | 597   | <a href="https://www.blueletterbible.org/lexicon/h430/esv/wlc/0-1/">h430</a>                                                                              |         |
| אדני  | 587   | <a href="https://www.blueletterbible.org/lexicon/h136/esv/wlc/0-1/">h136</a>                                                                              |         |
| דוד   | 583   | <a href="https://www.blueletterbible.org/lexicon/h1730/esv/wlc/0-1/">h1730</a>                                                                            |         |
| אתה   | 582   | <a href="https://www.blueletterbible.org/lexicon/h857/esv/wlc/0-1/">h857</a>                                                                              |         |
| עם    | 582   | <a href="https://www.blueletterbible.org/lexicon/h5973/esv/wlc/0-1/">h5973</a>                                                                            |         |


### Torah {#torah}

{{< figure src="/ox-hugo/lhlg_torah_wc.png" >}}

| Word  | Count | Strong                                                                 |
|-------|-------|------------------------------------------------------------------------|
| את    | 2569  | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| אשר   | 1617  | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| יהוה  | 1493  | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| אל    | 1241  | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| על    | 949   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| כל    | 921   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| כי    | 895   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| לא    | 861   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| ואת   | 809   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| בני   | 620   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| ויאמר | 619   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| משה   | 598   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| ישראל | 508   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| ס     | 491   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| הוא   | 420   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| הארץ  | 352   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| ולא   | 345   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |
| לו    | 330   | <a href="https://www.blueletterbible.org/lexicon/h/esv/wlc/0-1/">h</a> |


## Experiments organized by books {#experiments-organized-by-books}


### Genesis {#genesis}

{{< figure src="/ox-hugo/genesis.png" >}}

| Word  | Count | Strong                                                                         |
|-------|-------|--------------------------------------------------------------------------------|
| את    | 658   | <a href="https://www.blueletterbible.org/lexicon/h854/esv/wlc/0-1/">h854</a>   |
| אשר   | 351   | <a href="https://www.blueletterbible.org/lexicon/h834/esv/wlc/0-1/">h834</a>   |
| ויאמר | 337   | <a href="https://www.blueletterbible.org/lexicon/h559/esv/wlc/0-1/">h559</a>   |
| אל    | 335   | <a href="https://www.blueletterbible.org/lexicon/h410/esv/wlc/0-1/">h410</a>   |
| כי    | 263   | <a href="https://www.blueletterbible.org/lexicon/h3588/esv/wlc/0-1/">h3588</a> |
| ואת   | 205   | <a href="https://www.blueletterbible.org/lexicon/h854/esv/wlc/0-1/">h854</a>   |
| על    | 203   | <a href="https://www.blueletterbible.org/lexicon/h5921/esv/wlc/0-1/">h5921</a> |
| כל    | 199   | <a href="https://www.blueletterbible.org/lexicon/h3605/esv/wlc/0-1/">h3605</a> |
| אלהים | 150   | <a href="https://www.blueletterbible.org/lexicon/h430/esv/wlc/0-1/">h430</a>   |
| יוסף  | 144   | <a href="https://www.blueletterbible.org/lexicon/h3130/esv/wlc/0-1/">h3130</a> |
| יעקב  | 142   | <a href="https://www.blueletterbible.org/lexicon/h3290/esv/wlc/0-1/">h3290</a> |
| יהוה  | 141   | <a href="https://www.blueletterbible.org/lexicon/h3068/esv/wlc/0-1/">h3068</a> |
| לא    | 137   | <a href="https://www.blueletterbible.org/lexicon/h3809/esv/wlc/0-1/">h3809</a> |
| הארץ  | 126   | <a href="https://www.blueletterbible.org/lexicon/h776/esv/wlc/0-1/">h776</a>   |
| לו    | 126   | <a href="https://www.blueletterbible.org/lexicon/h3863/esv/wlc/0-1/">h3863</a> |
| ויהי  | 125   | <a href="https://www.blueletterbible.org/lexicon/h1961/esv/wlc/0-1/">h1961</a> |
| בני   | 118   | <a href="https://www.blueletterbible.org/lexicon/h1123/esv/wlc/0-1/">h1123</a> |
| הוא   | 110   | <a href="https://www.blueletterbible.org/lexicon/h1931/esv/wlc/0-1/">h1931</a> |
| אברהם | 108   | <a href="https://www.blueletterbible.org/lexicon/h85/esv/wlc/0-1/">h85</a>     |
| שנה   | 102   | <a href="https://www.blueletterbible.org/lexicon/h8141/esv/wlc/0-1/">h8141</a> |


## Conclusion {#conclusion}

The more I try to know, the less I feel I know... This coding-linguistic area is
fascinating and intreaguing. I feel like a taxonomy of this area is needed.

This will probaly take a lot of time, which I don't have. So I want to try to
balance between being effective and being efficient. I cannot afford to spend
much time with this project but on the other hand the Hebrew language is really
really apealing to me. I have yet to find a balance on how to proceed.

As part of documenting things I am going to include some very interesting
resources that I have found.

From the time I spent researching I would probably go with the BHSA DB. This is
a proposed roadmap:

-   Check the ETCBC/course materials for an introduction
-   Check the BHSA/bigTables for Pandas examples and learn how to use it
-   Reevaluate then

Also, if focus is more in learning Hebrew, check the Parabible website which
looks really lean and straigthforward.


### [Bible in Hebrew Demo](https://www.bibleinhebrew.com/bih/BiH_demo.php?uid=10) {#bible-in-hebrew-demo}

-   Source: <https://www.bibleinhebrew.com/bih/BiH_demo.php?uid=10>
-   Title: Bible in Hebrew Demo
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [See, Hear, and Read the Bible in Hebrew - BibleinHebrew.com](https://www.bibleinhebrew.com/bih/) {#see-hear-and-read-the-bible-in-hebrew-bibleinhebrew-dot-com}

-   Source: <https://www.bibleinhebrew.com/bih/>
-   Title: See, Hear, and Read the Bible in Hebrew - BibleinHebrew.com
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [Parabible | Genesis 1](https://parabible.com/Genesis/1) {#parabible-genesis-1}

-   Source: <https://parabible.com/Genesis/1>
-   Title: Parabible | Genesis 1
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [GitHub - jcuenod/awesome-bible-data: 😎 A curated list of generously licensed Bible data.](https://github.com/jcuenod/awesome-bible-data) {#github-jcuenod-awesome-bible-data-a-curated-list-of-generously-licensed-bible-data-dot}

-   Source: <https://github.com/jcuenod/awesome-bible-data>
-   Title: GitHub - jcuenod/awesome-bible-data: 😎 A curated list of generously licensed Bible data.
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [Text-Fabric versus SHEBANQ - BHSA](https://etcbc.github.io/bhsa/mql/) {#text-fabric-versus-shebanq-bhsa}

-   Source: <https://etcbc.github.io/bhsa/mql/>
-   Title: Text-Fabric versus SHEBANQ - BHSA
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [GitHub - ETCBC/course_materials: Contains scripts to learn to work with the ETCBC database](https://github.com/ETCBC/course_materials) {#github-etcbc-course-materials-contains-scripts-to-learn-to-work-with-the-etcbc-database}

-   Source: <https://github.com/ETCBC/course_materials>
-   Title: GitHub - ETCBC/course_materials: Contains scripts to learn to work with the ETCBC database
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [bhsa/bigTablesP.ipynb at master · ETCBC/bhsa · GitHub](https://github.com/ETCBC/bhsa/blob/master/programs/bigTablesP.ipynb) {#bhsa-bigtablesp-dot-ipynb-at-master-etcbc-bhsa-github}

-   Source: <https://github.com/ETCBC/bhsa/blob/master/programs/bigTablesP.ipynb>
-   Title: bhsa/bigTablesP.ipynb at master · ETCBC/bhsa · GitHub
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### Shebanq / Words [Words](https://shebanq.ancient-data.org/hebrew/words) {#shebanq-words-words}

-   Source: <https://shebanq.ancient-data.org/hebrew/words>
-   Title: Words
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### Shebanq / text [[2017] Genesis 30:1](https://shebanq.ancient-data.org/hebrew/text) {#shebanq-text-2017-genesis-30-1}

-   Source: <https://shebanq.ancient-data.org/hebrew/text>
-   Title: [2017] Genesis 30:1
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [Jupyter Notebook Viewer](https://nbviewer.org/github/etcbc/bhsa/blob/master/tutorial/search.ipynb) {#jupyter-notebook-viewer}

-   Source: <https://nbviewer.org/github/etcbc/bhsa/blob/master/tutorial/search.ipynb>
-   Title: Jupyter Notebook Viewer
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### [OSHB Read](https://hb.openscriptures.org/read/) {#oshb-read}

-   Source: <https://hb.openscriptures.org/read/>
-   Title: OSHB Read
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [OSHB Lexicon](http://openscriptures.github.io/HebrewLexicon/HomeFiles/Lexicon.html) {#oshb-lexicon}

-   Source: <http://openscriptures.github.io/HebrewLexicon/HomeFiles/Lexicon.html>
-   Title: OSHB Lexicon
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [Bible Online Learner](https://booge.eu/) {#bible-online-learner}

-   Source: <https://booge.eu/>
-   Title: Bible Online Learner
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [J. Ted Blakley — Online Hebrew Resources](https://www.blakleycreative.com/jtb/HebrewOnline.htm) {#j-dot-ted-blakley-online-hebrew-resources}

-   Source: <https://www.blakleycreative.com/jtb/HebrewOnline.htm>
-   Title: J. Ted Blakley — Online Hebrew Resources
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>


### Tanach.us text files webservice [Text files](https://www.tanach.us/Pages/TextFiles.html) {#tanach-dot-us-text-files-webservice-text-files}

-   Source: <https://www.tanach.us/Pages/TextFiles.html>
-   Title: Text files
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-06 Mon]</span></span>
-   Command: wget --user-agent=" Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36" "<http://tanach.us/Server.txt?Deuteronomy26:1-1&layout=Text-only&content=Consonants>"


### [Removing Vowels from Hebrew Unicode Text · GitHub](https://gist.github.com/yakovsh/345a71d841871cc3d375) {#removing-vowels-from-hebrew-unicode-text-github}

-   Source: <https://gist.github.com/yakovsh/345a71d841871cc3d375>
-   Title: Removing Vowels from Hebrew Unicode Text · GitHub
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [shoroshim.pdf](https://halakhah.com/rst/shoroshim.pdf) {#shoroshim-dot-pdf}

-   Source: <https://halakhah.com/rst/shoroshim.pdf>
-   Title:
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>


### [unfoldingWord® Hebrew Grammar — unfoldingWord® Hebrew Grammar 1 documentation](https://uhg.readthedocs.io/en/latest/front.html) {#unfoldingword-hebrew-grammar-unfoldingword-hebrew-grammar-1-documentation}

-   Source: <https://uhg.readthedocs.io/en/latest/front.html>
-   Title: unfoldingWord® Hebrew Grammar — unfoldingWord® Hebrew Grammar 1 documentation
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-06-05 Sun]</span></span>
