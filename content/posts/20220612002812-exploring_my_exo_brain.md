+++
title = "Exploring my [exo]brain"
author = ["Albert De La Fuente Vigliotti"]
date = 2022-06-12
lastmod = 2023-08-13T11:05:12-03:00
tags = ["exobrain", "digitalgardens", "research", "graphs", "communities", "clustering"]
categories = ["emacs", "visualization"]
draft = false
image = "/images/fm_exploring_exobrain.png"
featured_image = "/~albert/exploring-exobrain.png"
+++

I really like working with graphs. I remember a project that I worked a while
ago where I parsed the client requirements relationships into a graph and I
could determine the priorities of modules development based on that. Good
souvenirs. This original idea came from an analysis I did on my social
networks out of curiosity and I could identify and [find communities on graphs]({{< relref "20220109211521-finding_communities_on_graphs.md" >}}).

Now I am using org-roam as my notes management app. It is great and it works on
top of org-mode and of course Emacs. Not everyone feels comfortable with this
since it requires to be okay with technical bits and bytes. I just love it...

I started using v1.0 which did not supported to map org headings but only file
top headers. This was brilliant already enough in itself. With v2.0 org-roam
included the ability to map headers also. What was good became even better! The
only caveat at that time is that org-roam-server stopped working. That is
understandable. Now there is a new project called org-roam-ui which is basically
org-roam-server for v2.0 with headings support.

Recently I trimmed my DB, I am trying to find a balance between using headings
and not using headings. My Emacs default config used to creat UUIDs for each
heading. This was good at first but I had to adapt this for org-roam v2.0... I
had +9000 notes, which is unpractical to be honest... So I cleared the UUIDs of
the headers of several files that were not needed and now I have +3000 notes. It
is still too much IMO, so further trimming is needed but that cleanup needs more
thought.

Now lets preview this thing... Most screenshots have been anonimized.  At the
time of this writing this is how my notes looks like. Keep in mind that out of
all of my notes, only a few selected ones are shared here on my website.

I am going to show first a little video sample

{{< youtube YCaotPnKU5w >}}

Figure [1](#figure--fig:full-headings) is how the exobrain looks considering all notes

<a id="figure--fig:full-headings"></a>

{{< figure src="/ox-hugo/exploring-exobrain-full-headings.png" caption="<span class=\"figure-number\">Figure 1: </span>Exobrain with all headings and subheadings" >}}

Figure [2](#figure--fig:only-headers) is how the exobrain looks only considering the file headers and subheadings.

<a id="figure--fig:only-headers"></a>

{{< figure src="/ox-hugo/exploring-exobrain-file-headers-only.png" caption="<span class=\"figure-number\">Figure 2: </span>Exobrain with all headings and subheadings" >}}

Figure [2](#figure--fig:only-headers) shows my Emacs configuration file cluster and how it relates to other notes.

<a id="figure--fig:doom-emacs-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-doom-emacs-config-cluster.png" caption="<span class=\"figure-number\">Figure 3: </span>Exobrain highlighting Doom Emacs related notes" >}}

Figure [4](#figure--fig:parashot-notes) shows my parashot study notes.

<a id="figure--fig:parashot-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-torah-parashots-cluster.png" caption="<span class=\"figure-number\">Figure 4: </span>Exobrain highlighting the Torah parashot notes" >}}

Figure [5](#figure--fig:beekeeping-notes)
shows my beekeeping notes and log, which is quite extensive by the way.

<a id="figure--fig:beekeeping-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-beekeeping-cluster.png" caption="<span class=\"figure-number\">Figure 5: </span>Exobrain highlighting the beekeping notes and logs" >}}

Figure [6](#figure--fig:trading-testing-notes)
shows my testing trading notes. I developed my own tool to document my simulated tradings and tests.

<a id="figure--fig:trading-testing-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-trading-testing-journal-cluster.png" caption="<span class=\"figure-number\">Figure 6: </span>Exobrain highlighting the testing version of my trading log (development purposes)" >}}

Figure [6](#figure--fig:trading-testing-notes) shows my simulated trading log. I integrated my
own python tool with yasnippets, so Emacs could get all the goodies out of it.
The result is a very detailed (and possibly overkill) simulated trade log. This
is going to probably be decoupled from my org-roam database. I don't really need
to search on all of that.

<a id="figure--fig:trading-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-trading-journal-cluster.png" caption="<span class=\"figure-number\">Figure 7: </span>Exobrain highlighting my trading log" >}}

Figure [8](#figure--fig:bots-notes)
shows my research notes on automated trading strategies.

<a id="figure--fig:bots-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-trading-bots-research-cluster.png" caption="<span class=\"figure-number\">Figure 8: </span>Exobrain highlighting my trading bots strategies and backtesting research" >}}

Figure [9](#figure--fig:agroforestry-notes)
shows my research notes on agroforestry.

<a id="figure--fig:agroforestry-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-agroforestry-cluster.png" caption="<span class=\"figure-number\">Figure 9: </span>Exobrain highlighting the agroforestry notes, design and log" >}}

Figure [10](#figure--fig:journal-2022-notes)
shows my journal of the current year 2022. TODO's tasks and how they integrate with the rest of the notes.

<a id="figure--fig:journal-2022-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-y2022-journal-log-cluster.png" caption="<span class=\"figure-number\">Figure 10: </span>Exobrain highlighting the 2022 journal log" >}}

Figure [11](#figure--fig:homestead-notes)
shows my homesteading notes and all the ongoing projects.

<a id="figure--fig:homestead-notes"></a>

{{< figure src="/ox-hugo/exploring-exobrain-homestead-cluster.png" caption="<span class=\"figure-number\">Figure 11: </span>Exobrain highlighting the homestead related notes" >}}
