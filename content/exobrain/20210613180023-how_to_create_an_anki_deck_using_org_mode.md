+++
title = "How to create an anki deck using org-mode"
author = ["Albert De La Fuente Vigliotti"]
date = 2021-06-13
lastmod = 2022-01-11T23:33:19-03:00
tags = ["emacs", "org-mode", "ankidecks"]
categories = ["emacs", "org-mode", "ankidecks"]
draft = false
+++

-   Install anki
-   Login
-   Install the add-on on anki (<https://ankiweb.net/shared/info/2055492159>)
    -   Ctrl + Shift + A
    -   Click: Get Add-ons
    -   Paste the code: 2055492159
-   Restart Anki

On org-mode

-   M-x `anki-editor-mode`
-   Go to the org-mode file that has the deck you want to create
-   M-x `anki-editor-push-notes`

Publish

See also: <https://rgoswami.me/posts/anki-decks-orgmode/>
