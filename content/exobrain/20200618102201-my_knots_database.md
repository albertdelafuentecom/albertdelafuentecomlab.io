+++
title = "My knots database"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-06-18
lastmod = 2022-01-11T23:28:19-03:00
tags = ["survival", "knots", "ankidecks"]
categories = ["survival"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Vonpupp's top knots](#vonpupp-s-top-knots)
    - [TOP 10](#top-10)
        - [<span class="org-todo done DONE">DONE</span> Chain sinnet / Daisy chain](#chain-sinnet-daisy-chain):quickrelease:
        - [<span class="org-todo done DONE">DONE</span> Coil unattached rope](#coil-unattached-rope):canterbury:quickrelease:
        - [<span class="org-todo done DONE">DONE</span> (MOD) Marlinespike](#mod--marlinespike):canterbury:slidegrip:hitches:quickrelease:
        - [<span class="org-todo done DONE">DONE</span> (MOD) The truckers hitch with rolling hitch](#mod--the-truckers-hitch-with-rolling-hitch):miranda:canterbury:hitches:
        - [<span class="org-todo done DONE">DONE</span> The bowline](#the-bowline):miranda:canterbury:endloop:
        - [<span class="org-todo done DONE">DONE</span> Double fisherman's bend](#double-fisherman-s-bend):canterbury:bends:endloop:
        - [<span class="org-todo done DONE">DONE</span> (MOD) Prusic knot w/double fisherman's bend](#mod--prusic-knot-w-double-fisherman-s-bend):miranda:canterbury:slidegrip:hitches:
        - [<span class="org-todo done DONE">DONE</span> Mooring hitch](#mooring-hitch):quickrelease:hitches:
        - [<span class="org-todo done DONE">DONE</span> Clove hitch / constrictor](#clove-hitch-constrictor):miranda:hitches:
        - [<span class="org-todo done DONE">DONE</span> Alpine butterfly](#alpine-butterfly):midloop:
        - [new knot](#new-knot)
    - [TOP 20](#top-20)
        - [<span class="org-todo todo TODO">TODO</span> Shear lashing](#shear-lashing)
        - [<span class="org-todo todo TODO">TODO</span> Tripod lashing](#tripod-lashing)
        - [<span class="org-todo todo TODO">TODO</span> Diagonal lashing](#diagonal-lashing)
        - [<span class="org-todo todo TODO">TODO</span> Square lashing](#square-lashing)
        - [<span class="org-todo todo TODO">TODO</span> Handcuff knot](#handcuff-knot):midloop:@searchrescue:
        - [<span class="org-todo todo TODO">TODO</span> Double overhand stopper](#double-overhand-stopper):stopper:
        - [<span class="org-todo todo TODO">TODO</span> The constrictor knot (loop method)](#the-constrictor-knot--loop-method)
        - [<span class="org-todo todo TODO">TODO</span> Barrel hitch](#barrel-hitch)
        - [<span class="org-todo todo TODO">TODO</span> Snell knot](#snell-knot):@fishing:
- [Discarded knots](#discarded-knots)
    - [DISCARD (MOD) The double half-hitch with a bite?](#discard--mod--the-double-half-hitch-with-a-bite):quickrelease:hitches:
    - [DISCARD Heaving line](#discard-heaving-line):@arborist:
    - [(?) Slip knot](#slip-knot):quickrelease:

</div>
<!--endtoc-->

confidence
: certain

importance
: 9/10


## Vonpupp's top knots {#vonpupp-s-top-knots}

This document is a summary of the knots that I consider useful for bushcraft and
survival. This document is in fact crafted to be presented as an Anki deck
that can be [downloaded here](</home/av/org-media/files/Vonpupp_ Top nos.apkg>).

Be aware that the Anki deck information is presented in Portuguese, while the
Org core information is in English (hence the english/portuguese mess)


### TOP 10 {#top-10}


#### <span class="org-todo done DONE">DONE</span> Chain sinnet / Daisy chain {#chain-sinnet-daisy-chain}

<!--list-separator-->

-  Front

    Forma de guardar uma corda reduzindo a 1/3 o seu comprimento

<!--list-separator-->

-  Back

    -   EN: Chain sinnet / Daisy chain

    {{< figure src="/ox-hugo/chainsinnetR17.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/chain-sinnet-knot>
    -   <https://youtu.be/w1CNRzZ4aI0>


#### <span class="org-todo done DONE">DONE</span> Coil unattached rope {#coil-unattached-rope}

<!--list-separator-->

-  Front

    Forma de guardar uma corda sem reduzir o seu comprimento

<!--list-separator-->

-  Back

    -   EN: Coil unattached rope

    {{< figure src="/ox-hugo/coilingR21.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/coil-unattached-rope-knot>
    -   <https://youtu.be/qMGf15BJxU0>


#### <span class="org-todo done DONE">DONE</span> (MOD) Marlinespike {#mod--marlinespike}

<!--list-separator-->

-  Front

    -   Escada
    -   Segura varas de barracas
    -   Regulagem de linha de tarp
    -   Pendurar mochila de arvore

<!--list-separator-->

-  Back

    -   EN: Marlingspike

    {{< figure src="/ox-hugo/marlinspikeR15.jpg" width="150%" height="150%" >}}

    -   (MOD) <https://www.youtube.com/watch?v=VUerD-QvFnc>
    -   <https://www.animatedknots.com/marlinspike-hitch-knot>
    -   <https://youtu.be/MR61BYTQMwo>


#### <span class="org-todo done DONE">DONE</span> (MOD) The truckers hitch with rolling hitch {#mod--the-truckers-hitch-with-rolling-hitch}

<!--list-separator-->

-  Front

    -   Segurar objetos
    -   Efeito poleia

<!--list-separator-->

-  Back

    -   EN: (MOD) The truckers hitch with rolling hitch

    {{< figure src="/ox-hugo/truckersR11.jpg" width="150%" height="150%" >}}

    -   (MOD) <https://www.youtube.com/watch?v=VUerD-QvFnc>
    -   <https://www.animatedknots.com/truckers-hitch-knot>
    -   <https://youtu.be/8N6blmw006U>


#### <span class="org-todo done DONE">DONE</span> The bowline {#the-bowline}

<!--list-separator-->

-  Front

    -   Loop facil de desfazer

<!--list-separator-->

-  Back

    -   EN: The Bowline

    <img src="/ox-hugo/bowlineR7.jpg" alt="bowlineR7.jpg" width="150%" height="150%" />
    <https://www.animatedknots.com/bowline-knot>
    <https://youtu.be/YXRnPES0Qec>


#### <span class="org-todo done DONE">DONE</span> Double fisherman's bend {#double-fisherman-s-bend}

<!--list-separator-->

-  Front

    -   Unir duas cordas
    -   Fazer stopper
    -   Fazer laço ajustavel

<!--list-separator-->

-  Back

    -   EN: Double fisherman's bend

    {{< figure src="/ox-hugo/doublefishermansR16.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/double-fishermans-bend-knot>
    -   <https://youtu.be/O6oJwedcb18>


#### <span class="org-todo done DONE">DONE</span> (MOD) Prusic knot w/double fisherman's bend {#mod--prusic-knot-w-double-fisherman-s-bend}

<!--list-separator-->

-  Front

    -   Fazer amarraçoes num tarp que permitem deslizar e travar

<!--list-separator-->

-  Back

    -   EN: (MOD) Prusic knot w/double fisherman's bend

    {{< figure src="/ox-hugo/prusikR8.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/prusik-knot>
    -   <https://youtu.be/CP7iAF%5FYU7A>


#### <span class="org-todo done DONE">DONE</span> Mooring hitch {#mooring-hitch}

<!--list-separator-->

-  Front

    -   Hitch mais seguro que permite desfazer rapido

<!--list-separator-->

-  Back

    -   EN: Mooring hitch

    {{< figure src="/ox-hugo/mooringR9.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/mooring-hitch-knot>
    -   <https://youtu.be/EOEOAaMY%5FUo>


#### <span class="org-todo done DONE">DONE</span> Clove hitch / constrictor {#clove-hitch-constrictor}

<!--list-separator-->

-  Front

    -   Segurar varias varas de pesca juntas
    -   Puxar (ou elevar) um tronco ou cano
    -   Similar ao constritor, mas pode chegar a escorregar

<!--list-separator-->

-  Back

    -   EN: Clove hitch / constrictor

    {{< figure src="/ox-hugo/cloveR6.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/clove-hitch-knot-using-loops>
    -   <https://youtu.be/pMRTmEZ4-JU>


#### <span class="org-todo done DONE">DONE</span> Alpine butterfly {#alpine-butterfly}

<!--list-separator-->

-  Front

    -   Loop no meio da corda
    -   Usado na escalada tambem

<!--list-separator-->

-  Back

    -   EN: Alpine butterfly

    {{< figure src="/ox-hugo/alpinebutterflyR12.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/alpine-butterfly-loop-knot>#
    -   <https://www.youtube.com/gX1dWKg6Ttc>


#### new knot {#new-knot}


### TOP 20 {#top-20}


#### <span class="org-todo todo TODO">TODO</span> Shear lashing {#shear-lashing}

<!--list-separator-->

-  Front

    -   Amarracao de dois estacas em forma de A

<!--list-separator-->

-  Back

    -   EN: Shear lashing

    {{< figure src="/ox-hugo/lashshearR11.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/shear-lashing-knot>
    -   <https://www.youtube.com/embed/qAS7LO7Vsps?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Tripod lashing {#tripod-lashing}

<!--list-separator-->

-  Front

    -   Amarracao de tres estacas em forma de tripe

<!--list-separator-->

-  Back

    -   EN: Tripod lashing

    {{< figure src="/ox-hugo/lashtripodR16.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/tripod-lashing-knot>
    -   <https://www.youtube.com/embed/C9Rk90zNJpM?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Diagonal lashing {#diagonal-lashing}

<!--list-separator-->

-  Front

    -   Amarracao de duas estacas em forma diagonal

<!--list-separator-->

-  Back

    -   EN: Diagonal lashing

    {{< figure src="/ox-hugo/lashdiagonalR15.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/diagonal-lashing-knot>
    -   <https://www.youtube.com/embed/L-fF4BG0rlk?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Square lashing {#square-lashing}

<!--list-separator-->

-  Front

    -   Amarracao de duas estacas em angulo de 90 graus

<!--list-separator-->

-  Back

    -   EN: Square lashing

    {{< figure src="/ox-hugo/lashsquareR18.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/square-lashing-knot>
    -   <https://www.youtube.com/embed/3GVE0EPEc1o?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Handcuff knot {#handcuff-knot}

<!--list-separator-->

-  Front

    -   Prender maos ou pes

<!--list-separator-->

-  Back

    -   EN: Handcuff knot                                 :midloop:@searchrescue:

    {{< figure src="/ox-hugo/handcuffR9.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/handcuff-knot>
    -   <https://www.youtube.com/embed/6OS6hUnMbwI?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Double overhand stopper {#double-overhand-stopper}

<!--list-separator-->

-  Front

    -   Parar corda numa passagem estreita
    -   Stopper mais seguro que pode ser usado com outros nos

<!--list-separator-->

-  Back

    -   EN: Double overhand stopper                                     :stopper:

    <img src="/ox-hugo/doubleoverhandR8.jpg" alt="doubleoverhandR8.jpg" width="150%" height="150%" />
    <https://www.animatedknots.com/double-overhand-stopper-knot>
    <https://www.youtube.com/embed/7Eso8q84aCc?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> The constrictor knot (loop method) {#the-constrictor-knot--loop-method}

<!--list-separator-->

-  Front

    -   Segura uma estaca ou varios items juntos

<!--list-separator-->

-  Back

    -   EN: The constrictor knot (loop method)


#### <span class="org-todo todo TODO">TODO</span> Barrel hitch {#barrel-hitch}

<!--list-separator-->

-  Front

    -   Segura um balde ou lata

<!--list-separator-->

-  Back

    -   EN: Barrel hitch

    {{< figure src="/ox-hugo/barrelR8.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/barrel-hitch-knot>
    -   <https://www.youtube.com/embed/M3bdLFo7M98?modestbranding=1&rel=0&hd=1&wmode=transparent>


#### <span class="org-todo todo TODO">TODO</span> Snell knot {#snell-knot}

<!--list-separator-->

-  Front

    -   No para segurar anzol

<!--list-separator-->

-  Back

    -   EN: Snell knot                                                 :@fishing:

    {{< figure src="/ox-hugo/snellR12.jpg" width="150%" height="150%" >}}

    -   <https://www.animatedknots.com/snell-knot>
    -   <https://www.youtube.com/embed/5Gp56i4UfCE?modestbranding=1&rel=0&hd=1&wmode=transparent>


## Discarded knots {#discarded-knots}


### DISCARD (MOD) The double half-hitch with a bite? {#discard--mod--the-double-half-hitch-with-a-bite}

(MOD) <https://www.youtube.com/watch?v=pGW9qw-nqvc>
<https://www.animatedknots.com/two-half-hitches-knot>
![](https://www.animatedknots.com/photos/twohalfhitches/twohalfhitchesR6.jpg)

-   Similar to the Mooring hitch, which is actually better


### DISCARD Heaving line {#discard-heaving-line}

<https://www.animatedknots.com/heaving-line-knot>
![](https://www.animatedknots.com/photos/heavingline/heavinglineR15.jpg)
<https://www.youtube.com/embed/wMH4b1a0lVo?modestbranding=1&rel=0&hd=1&wmode=transparent>

-   Heavies the end of a rope to use it as a throw line
-   See Coil unattached rope


### (?) Slip knot {#slip-knot}

<https://www.animatedknots.com/slip-knot>
![](https://www.animatedknots.com/photos/slip/slipR6.jpg)
<https://youtu.be/bxnkth1n%5F2A>
