+++
title = "Hebrew root words - parent roots dictionary"
date = 2021-11-20
lastmod = 2022-01-11T23:32:05-03:00
tags = ["hebrew", "ankidecks"]
categories = ["hebrew"]
draft = false
+++

confidence
: certain

importance
: 9/10

If you are reading this from the website it is probably going to be a bit
uncomfortable to read. This is an export meant to be read in org-mode or as an
anki deck. This is still work in progress.


## Hebrew root words - parent roots dictionary {#hebrew-root-words-parent-roots-dictionary}


### AB {#ab}


##### Front {#front}

[latex]
\texthebrew{אב} / abh
[/latex]


##### Back {#back}

Stand / Tent Pole
[latex]
\texthebrew{אב} / abh
[/latex]


## Aleph \textproto{a} / \textproto{A}: strength {#aleph-textproto-a-textproto-a-strength}

| \textproto{ba} | \texthebrew{אב} | abh  | Stand / Tent Pole |
|----------------|-----------------|------|-------------------|
| \textproto{xa} | \texthebrew{א}  | ahh  | Protect / Hearth  |
| \textproto{la} | \texthebrew{א}  | el   | Yoke / Ox         |
| \textproto{ma} | \texthebrew{א}  | eym  | Blind / Glue      |
| \textproto{na} | \texthebrew{א}  | an   | Produce / Fig     |
| \textproto{va} | \texthebrew{א}  | eysh | Press / Fire      |
| \textproto{ta} | \texthebrew{א}  | et   | Plow / Mark       |


## Beyt \textproto{b} / \textproto{B}: house {#beyt-textproto-b-textproto-b-house}

| \textproto{ab} | \texthebrew{} | ba   | Come / Void        |
|----------------|---------------|------|--------------------|
| \textproto{db} | \texthebrew{} | bad  | Separate / Stick   |
| \textproto{xb} | \texthebrew{} | bahh | Slaughter / Knife  |
| \textproto{lb} | \texthebrew{} | bal  | Mix / Fodder       |
| \textproto{nb} | \texthebrew{} | ben  | Build / Tent panel |
| \textproto{rb} | \texthebrew{} | bar  | Feed / Grain       |
| \textproto{tb} | \texthebrew{} | bet  | Lodge / House      |


## Gimmel \textproto{g} / \textproto{G}: foot {#gimmel-textproto-g-textproto-g-foot}

| \textproto{bg} | \texthebrew{} | gabh | Lift / Arch        |
|----------------|---------------|------|--------------------|
| \textproto{dg} | \texthebrew{} | gad  | Slice / Fortune    |
| \textproto{wg} | \texthebrew{} | gaw  | Black              |
| \textproto{lg} | \texthebrew{} | gal  | Roll / Round       |
| \textproto{ng} | \texthebrew{} | gan  | Shield / Garden    |
| \textproto{rg} | \texthebrew{} | ger  | Sujourn / Traveler |


## References {#references}

<https://www.ancient-hebrew.org/roots-words/aleph.htm>

<https://www.ancient-hebrew.org/ahlb/aleph.html>
