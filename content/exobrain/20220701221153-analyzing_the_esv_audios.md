+++
title = "Analyzing the ESV audios"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-07-01T22:31:32-03:00
tags = ["bible", "esv"]
categories = ["python", "analysis"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Audios duration by book](#audios-duration-by-book)
- [Audios duration by chapter](#audios-duration-by-chapter)
- [Audios duration by verse](#audios-duration-by-verse)

</div>
<!--endtoc-->

This is an analysis of the mp3 files of the ESV audios to know their duration and create my own bible study plans / parashots.

Statistics of the ESV audio Scriptures:

-   Total verses: 31103
-   Audio duration in seconds: 262037.84
-   Audio duration in minutes: 4367.29
-   Audio duration in hours: 72.78
-   Audio duration in days: 3.03


## Audios duration by book {#audios-duration-by-book}

<iframe class="airtable-embed" src="https://airtable.com/embed/shreKjDkp0M0EtCcl?backgroundColor=cyan&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>


## Audios duration by chapter {#audios-duration-by-chapter}

<iframe class="airtable-embed" src="https://airtable.com/embed/shrxHinFIfT3LvGgn?backgroundColor=cyan&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>


## Audios duration by verse {#audios-duration-by-verse}

<iframe class="airtable-embed" src="https://airtable.com/embed/shrbPZdwOVbq7b5fe?backgroundColor=cyan&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>
