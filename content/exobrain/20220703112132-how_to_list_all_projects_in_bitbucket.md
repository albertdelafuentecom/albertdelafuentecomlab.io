+++
title = "How to list all projects in BitBucket"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-07-03T11:57:21-03:00
tags = ["gitlab", "cli"]
categories = ["arch", "linux"]
draft = false
+++

Related notes
: [How to list all projects in Gitlab]({{< relref "20220703004228-how_to_list_all_projects_in_gitlab.md" >}}), [How to list all projects in Github]({{< relref "20220703011205-how_to_list_all_projects_in_github.md" >}})

Install the bbcli with:

```sh
pacaur -S bbcli
```

Auth with:

```sh
bb auth login
```

At the writing of this, the tool doesn't seem to support oauth and bitbucket
seems to be enforcing it recently. So I am abandoning this. I think I will move
out of bitbucket anyway.
