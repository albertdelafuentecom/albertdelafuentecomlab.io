+++
title = "How to install interSubs on mpv to learn Hebrew"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-06-11T10:50:54-03:00
tags = ["mpv"]
categories = ["hebrew"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Setup](#setup)
- [Usage](#usage)
- [Troubleshooting errors](#troubleshooting-errors)
    - [Error of black screen](#error-of-black-screen)
    - [Reversed subs](#reversed-subs)
- [Video examples to watch that have hebrew subtitles](#video-examples-to-watch-that-have-hebrew-subtitles)
    - [Music](#music)
    - [Day to day modern hebrew](#day-to-day-modern-hebrew)
    - [Tanakh in hebrew](#tanakh-in-hebrew)

</div>
<!--endtoc-->

Back in the early 2000, I used to use [Babylon software](https://www.babylon-software.com/) a lot for translating
words (before I switched to Linux). It was very cool, it allowed to click on any
word in combination with a keystroke to get a popup with the translation of that
word.

I miss that feature much to be honest, specially with Hebrew. The [interSubs](https://github.com/oltodosel/interSubs)
plugin allows to do exactly that, but for the [mpv video player](https://mpv.io/) and using several
webservices.

Here is an example:

{{< youtube KkF9Sl_NgVc >}}

You can hover the mouse over different words and you will automagically get the
translation of that word.

Best of all, it integrates really well with my mpv configs, so I can open a
youtube video url on my notes system and it will automatically open with mpv and
have that feature enabled. The only caveat is that the video needs to have
Hebrew subtitles uploaded on Youtube.


## Setup {#setup}

Install packages

```sh
sudo pacman -S python-httpx python-lxml python-requests python-beautifulsoup4 python-numpy python-pyqt5 xcompmgr
```

Start the composite manager

```sh
xcompmgr -c
```

For extra documentation, check: <https://github.com/oltodosel/interSubs>


## Usage {#usage}

-   Start video with mpv &amp; select subtitles.
-   `F5` to start/stop interSubs.
-   Point cursor over the word to get popup with translation.
-   `F6` to hide/show without exiting.

Looping helps, use:

-   `l`: to enable looping (toggle)
-   `r`: to mark the start
-   `t`: to mark the end

You need to stop and play back within the loop interval (if I am not mistaken)

Then you can stop looping:

-   `R`: to remove the start mark
-   `T`: to remove the end mark


## Troubleshooting errors {#troubleshooting-errors}


### Error of black screen {#error-of-black-screen}

Symptoms:
Black regions on text, so no subs are displayed

{{< figure src="https://i.imgur.com/GFYrSKb.png" >}}

Solution:
Start the composite manager with

```sh
xcompmgr -c
```

Alternatively you could use `xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55 &`


### Reversed subs {#reversed-subs}

Symptoms:
The word translated does not match the word highlighted

{{< figure src="https://i.imgur.com/kRQBTNA.png" >}}

Solution:
Change the `R2L_from_B` flag to True.

```text
# translated language is written right-to-left, e.g Hebrew/Arabic
R2L_from_B = True
# translation is written right-to-left, e.g Hebrew/Arabic
R2L_to_B = False
```


## Video examples to watch that have hebrew subtitles {#video-examples-to-watch-that-have-hebrew-subtitles}


### Music {#music}

<https://www.youtube.com/watch?v=X2-5oKm5deY>

<https://www.youtube.com/watch?v=ZHW0uTpzsCM>

<https://www.youtube.com/watch?v=NcJwzmBFrKM>

<https://www.youtube.com/watch?v=-9wfi3MlJb4>

<https://www.youtube.com/watch?v=j1efB7WsjB0>

<https://www.youtube.com/watch?v=Kqmm6sXj_Zg>

<https://www.youtube.com/watch?v=--UABwqW9Sg>

<https://www.youtube.com/watch?v=Smvy8hvbBcA>

<https://www.youtube.com/watch?v=lxg4dcKH6aU>


### Day to day modern hebrew {#day-to-day-modern-hebrew}

<https://www.youtube.com/watch?v=_gHrC9J5WKE>


### Tanakh in hebrew {#tanakh-in-hebrew}

<https://www.youtube.com/watch?v=wDBhom68uHM&list=PLV-0AO4FcLqbypQztT1SB-YYT5ByD9Ulj&index=2>
