+++
title = "Testes de internet com sette fibra"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-06-24T16:54:07-03:00
tags = ["internet", "sette", "fibra"]
categories = ["tests"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Testes de banda](#testes-de-banda)
    - [Nperf com servidor nos Estados Unidos (Miami, Florida - US 10 Gb/s Movistar)](#nperf-com-servidor-nos-estados-unidos--miami-florida-us-10-gb-s-movistar)
    - [Nperf com servidor na Espanha (Aguilar de la Frontera, Andalucía - ES 10 Gb/s Ciberica)](#nperf-com-servidor-na-espanha--aguilar-de-la-frontera-andalucía-es-10-gb-s-ciberica)
    - [Nperf com servidor na Alemanha (Limburg, Hessen - DE 10 Gb/s OVH.com)](#nperf-com-servidor-na-alemanha--limburg-hessen-de-10-gb-s-ovh-dot-com)

</div>
<!--endtoc-->



## Testes de banda {#testes-de-banda}


### Nperf com servidor nos Estados Unidos (Miami, Florida - US 10 Gb/s Movistar) {#nperf-com-servidor-nos-estados-unidos--miami-florida-us-10-gb-s-movistar}

{{< figure src="https://pic.nperf.com/r/3391692585300091-0HALMtBj.png" >}}

<https://www.nperf.com/en/r/3391692585300091-0HALMtBj>


### Nperf com servidor na Espanha (Aguilar de la Frontera, Andalucía - ES 10 Gb/s Ciberica) {#nperf-com-servidor-na-espanha--aguilar-de-la-frontera-andalucía-es-10-gb-s-ciberica}

{{< figure src="https://pic.nperf.com/r/3391693262202431-AjnOspgg.png" >}}

<https://www.nperf.com/r/3391693262202431-AjnOspgg>


### Nperf com servidor na Alemanha (Limburg, Hessen - DE 10 Gb/s OVH.com) {#nperf-com-servidor-na-alemanha--limburg-hessen-de-10-gb-s-ovh-dot-com}

{{< figure src="https://pic.nperf.com/r/3391691962357631-2971kErs.png" >}}

<https://www.nperf.com/r/3391691962357631-2971kErs>
