+++
title = "Video notes: 8 common patterns in trauma bonded relationships"
author = ["Albert De La Fuente Vigliotti"]
date = 2021-03-27
lastmod = 2022-01-11T23:34:36-03:00
tags = ["psych", "trauma"]
categories = ["psych"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [8 common patterns in trauma bonded relationships - YouTube](#8-common-patterns-in-trauma-bonded-relationships-youtube)
    - [(1) Justification](#1--justification)
    - [(2) Believing the future faking](#2--believing-the-future-faking)
    - [(3) Keep having the same fight over and over (repetition compulsion)](#3--keep-having-the-same-fight-over-and-over--repetition-compulsion)
    - [(4) Not able to give clear reasons on staying in the relationship](#4--not-able-to-give-clear-reasons-on-staying-in-the-relationship)
    - [(5) Fear of leaving](#5--fear-of-leaving)
    - [(6) Becoming a one-stop-shop for your partner](#6--becoming-a-one-stop-shop-for-your-partner)
    - [(7) Hiding your feeling and needs](#7--hiding-your-feeling-and-needs)
    - [(8) Rationalizing the relationship to other people or hiding the pattern from others](#8--rationalizing-the-relationship-to-other-people-or-hiding-the-pattern-from-others)
- [Can this change?](#can-this-change)

</div>
<!--endtoc-->



## [8 common patterns in trauma bonded relationships - YouTube](https://www.youtube.com/watch?v=yH-H4PkoH88) {#8-common-patterns-in-trauma-bonded-relationships-youtube}

-   Source: <https://www.youtube.com/watch?v=yH-H4PkoH88>
-   Title: 8 common patterns in trauma bonded relationships - YouTube
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2021-03-27 Sat]</span></span>

Research more on "trauma bonded relationships", that is the term

Trauma bonded relationships are a form of indoctrination similar to a cult.
The way a cult keeps people around is by creating fear of what is outside in the unknown


### (1) Justification {#1--justification}

-   They don't mean it
-   My parent had a tough childhood
-   Starts in childhood trying to justify parent's abuse
-   It becomes a reflexive pattern in adults


### (2) Believing the future faking {#2--believing-the-future-faking}

-   I will go to the doctor
-   I am going to get therapy / I am going to work on myself
-   It never happens in a substantial way, and you get trapped
-   Empats want to believe and will get hanged to any branch that brings hope
-   The person lives in the hope, but never in the reality


### (3) Keep having the same fight over and over (repetition compulsion) {#3--keep-having-the-same-fight-over-and-over--repetition-compulsion}

-   It can be about money
-   Or not spending enough time together
-   About your respectives families
-   Same fights every holiday season
-   Same fight over and over with no resolution
-   Narcs have no intention on changing
-   Shows lacks of empathy or growth on narcs
-   Empats are having the same argument with hope of change
-   Toxic personalities are really tough and do not change


### (4) Not able to give clear reasons on staying in the relationship {#4--not-able-to-give-clear-reasons-on-staying-in-the-relationship}

-   It is "something"
-   Magical justification
-   You should be looking for hard and heavy stuff: Respect, kindness, compassion, mutuality, reciprocity, empathy, growth


### (5) Fear of leaving {#5--fear-of-leaving}

-   Once the empat recognize it, and wants to leave, a fear overcomes him
-   The most common themes are: Self doubt, "what if I am wrong"


### (6) Becoming a one-stop-shop for your partner {#6--becoming-a-one-stop-shop-for-your-partner}

-   "If I do enough for them, they will be happy". No they will never be happy, because they are not capable
-   Becoming a personal assistant with many roles like:
    -   Parent
    -   Chef
    -   Home assistant
    -   Cheerleader


### (7) Hiding your feeling and needs {#7--hiding-your-feeling-and-needs}

-   You do not feel that you can share your needs
-   It is true, you can't share without getting hurt
-   This pattern is not about survival, but self devaluation and fear of upsetting the narc


### (8) Rationalizing the relationship to other people or hiding the pattern from others {#8--rationalizing-the-relationship-to-other-people-or-hiding-the-pattern-from-others}

-   "That is normal, right"
-   There is a lot of shame, so it is not possible for an empat to talk to others about the relationship in happy terms
-   An empat can describe the relationship as good, and talk about the future fake stuff as it is real
-   Hide the unconfortable stuff or be very vague when talking about the relationship
-   Many empats do not get the support needed since they are offering a "good picture" of their lives
-   They might not be hearing the dissenting voices, empats are stuck! The patterns of justifications keep going


## Can this change? {#can-this-change}

-   It is very effortful for this to change
-   This is why when the empat meets a person that is calm, stable and without drama they might "not have a connection"
-   The absence of chaos is not recognized as normal. It takes a while to let go those patterns of childhood:
    -   Drama = love
    -   Abuse = love
    -   Invalidation = love
-   It requires to understand the architecture of these relationship and yourself
-   There is so much shame and subsequent self-blame
    -   It is not your fault, it is not understanding it
-   Once you understand it, you can change
