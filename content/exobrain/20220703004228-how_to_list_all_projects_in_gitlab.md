+++
title = "How to list all projects in Gitlab"
author = ["Albert De La Fuente Vigliotti"]
date = 2022-07-03
lastmod = 2023-08-15T00:26:41-03:00
tags = ["gitlab", "cli"]
categories = ["arch", "linux"]
draft = false
+++

I have tested the glab tool and apparently there [should be a way to list all
projects](https://github.com/profclems/glab/issues/671) but I couldn't do it. Maybe the arch linux package is still not
updated and there is no aur package, so... I searched for an alternative and
luckily there is always a python tool/module to help us out... so I installed
[python-gitlab-cli](https://python-gitlab.readthedocs.io/en/stable/cli-usage.html). Setup is pretty straight forward, just read the docs.

Alternatively you can install the AUR package with:

```text
pacaur -S python-gitlab
```

The only consideration are:

-   Use a pass wrapper to asymmetrically GPG decrypt the token (for security)
-   Use a timeout higher than 5 if your connection sucks (as mine)

This is how my `~/.python-gitlab.cfg` file looks like:

```text
[global]
default = gitlab
ssl_verify = true
timeout = 5

[gitlab]
url = https://gitlab.com
private_token = helper: ~/bin/pass-python-gitlab.sh
api_version = 4
timeout = 30
```

Once configured, the magic line to list all the repos alphabetically is:

```sh
gitlab -o json project list --owned=1 --all | jq .[].path_with_namespace
gitlab -o json project list --owned=1 --all --per-page 300 | jq .[].path_with_namespace
```


## References {#references}


### [How do I access gitlab api using python-gitlab? - Stack Overflow](https://stackoverflow.com/questions/57469184/how-do-i-access-gitlab-api-using-python-gitlab) {#how-do-i-access-gitlab-api-using-python-gitlab-stack-overflow}

-   Source: <https://stackoverflow.com/questions/57469184/how-do-i-access-gitlab-api-using-python-gitlab>
-   Title: How do I access gitlab api using python-gitlab? - Stack Overflow
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2022-07-03 Sun]</span></span>
