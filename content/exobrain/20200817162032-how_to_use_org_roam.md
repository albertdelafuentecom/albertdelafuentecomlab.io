+++
title = "How to use org-roam"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-08-17
lastmod = 2022-01-11T20:40:37-03:00
tags = ["emacs", "org-mode", "org-roam"]
categories = ["emacs", "org-mode"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Org-Roam use](#org-roam-use)
    - [Workflow](#workflow)
        - [Components](#components)
        - [Processes](#processes)
        - [Reading / resources](#reading-resources)
    - [Setup](#setup)
    - [Topics that I need to get better at](#topics-that-i-need-to-get-better-at)
        - [Templates](#templates)
        - [Roam-protocol](#roam-protocol)
        - [Define my workflow](#define-my-workflow)
- [Brain / mind map](#brain-mind-map)

</div>
<!--endtoc-->

confidence
: 2/10

importance
: 9/10


## Org-Roam use {#org-roam-use}


### Workflow {#workflow}

<https://www.reddit.com/r/emacs/comments/gsv5np/care%5Fto%5Fshare%5Fconfigs%5Ffor%5Fhow%5Fyou%5Fuse%5Forgroam/>

<https://www.orgroam.com/manual/Appendix.html#Appendix>

<https://www.orgroam.com/manual/Note%5F002dtaking-Workflows.html#Note%5F002dtaking-Workflows>

<https://www.orgroam.com/manual/A-Brief-Introduction-to-the-Zettelkasten-Method.html#A-Brief-Introduction-to-the-Zettelkasten-Method>


#### Components {#components}

<!--list-separator-->

-  Fleeting notes/Journal (org-journal)

<!--list-separator-->

-  Refined notes (org-mode + org-roam) [org-roam-find-file]

<!--list-separator-->

-  Links/backlinks (org-mode + org-roam) [org-roam-insert]

    -   Tags
    -   Other notes
    -   Three Pass Technique / journal (future date) / project / etc

<!--list-separator-->

-  Tags

<!--list-separator-->

-  Search (deft)

<!--list-separator-->

-  Navigation

<!--list-separator-->

-  PARA Method


#### Processes {#processes}

<!--list-separator-->

-  Capturing

    Most of my capture in Roam goes straight into the Daily Note. It’s an easy jumping off point, kind of like an Inbox, where I can associate bits of information with existing notes, or create new notes from them.

<!--list-separator-->

-  Connecting (“In what context do I want to see this note again?")

<!--list-separator-->

-  Creating


#### Reading / resources {#reading-resources}

<!--list-separator-->

- <span class="org-todo todo TODO">TODO</span>  Read book: How to take Smart Notes

    <https://www.goodreads.com/book/show/34507927-how-to-take-smart-notes>

<!--list-separator-->

- <span class="org-todo done DONE">DONE</span>  The Zettelkasten Method - LessWrong 2.0

<!--list-separator-->

- <span class="org-todo todo TODO">TODO</span>  Building a Second Brain in Roam…And Why You Might Want To : RoamResearch

<!--list-separator-->

- <span class="org-todo done DONE">DONE</span>  Roam Research: Why I Love It and How I Use It - Nat Eliason (awesome post, read it again)

    <https://www.nateliason.com/blog/roam>

<!--list-separator-->

- <span class="org-todo todo TODO">TODO</span>  Adam Keesling’s Twitter Thread

<!--list-separator-->

- <span class="org-todo done DONE">DONE</span>  How To Take Smart Notes With Org-mode · Jethro Kuan

<!--list-separator-->

- <span class="org-todo done DONE">DONE</span>  Watch <https://www.youtube.com/watch?v=RvWic15iXjk>


### Setup {#setup}


### Topics that I need to get better at {#topics-that-i-need-to-get-better-at}


#### Templates {#templates}


#### Roam-protocol {#roam-protocol}


#### Define my workflow {#define-my-workflow}


## Brain / mind map {#brain-mind-map}

-   <https://superuser.com/questions/218932/what-is-the-best-mindmapper-for-emacs>
-   <https://github.com/semk/Org2OPML>
-   <https://github.com/theodorewiles/org-mind-map>
-   <https://www.freeplane.org/wiki/index.php/Home>
-   <https://www.freeplane.org/wiki/index.php/Add-ons%5F(install)>
