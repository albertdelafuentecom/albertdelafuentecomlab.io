+++
title = "How to generate a litecoin (LTC) paper wallet with BIP38 password offline"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-12-25
lastmod = 2022-01-11T20:49:40-03:00
tags = ["crypto", "wallet", "litecoin", "bip38"]
categories = ["crypto"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Resources](#resources)
- [**IMPORTANT** PSA: Using paper wallets, understanding change addresses. : Bitcoin](#important-psa-using-paper-wallets-understanding-change-addresses-dot-bitcoin)
- [Log](#log)

</div>
<!--endtoc-->

wget <https://github.com/litecoin-project/liteaddress.org/archive/master.zip>
unzip

-   Disconnect wifi
-   Open `bitaddress.org.html` file in firefox
-   Go to paper wallet
-   Check BIP38 encrypt
-   Type password

To verify

-   Disconnect wifi
-   Open `bitaddress.org.html` file in firefox
-   Go to wallet details
-   Type private key
-   Type BIP38 passphrase
-   Use public and private keys to import wallet in Exodus

To import the wallet into Jaxx (requires 0.00002728 LTC)

-   Open Jaxx
-   Open menu at the top right corner
-   Go to tools - paper wallet import
-   Select coin
-   Paste the decrypted BP38 **WIF** address


## Resources {#resources}


## **IMPORTANT** [PSA: Using paper wallets, understanding change addresses. : Bitcoin](https://www.reddit.com/r/Bitcoin/comments/1c9xr7/psa%5Fusing%5Fpaper%5Fwallets%5Funderstanding%5Fchange/) {#important-psa-using-paper-wallets-understanding-change-addresses-dot-bitcoin}

-   Source: <https://www.reddit.com/r/Bitcoin/comments/1c9xr7/psa%5Fusing%5Fpaper%5Fwallets%5Funderstanding%5Fchange/>
-   Title: PSA: Using paper wallets, understanding change addresses. : Bitcoin
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2020-12-25 Fri]</span></span>
-   <https://99bitcoins.com/bitcoin-wallet/paper/>
-   <https://support.decentral.ca/hc/en-us/articles/360021479354-How-do-I-transfer-digital-assets-from-a-paper-wallet-private-key-in-Jaxx-Liberty>-
-   <https://support.exodus.io/article/87-can-i-import-a-private-key#:~:text=First%2C%20navigate%20to%20the%20asset,update%20to%20our%20latest%20release>.
-   <https://docs.dash.org/pt/stable/wallets/paper.html>


## Log {#log}

-   [X] Enviei USD 1 para a paper wallet (MsUS)
-   [X] Importei a paper wallet no Jaxx (SJuf)
-   [X] Enviei USD 10 para a paper wallet (MsUS)
-   [X] Importei a paper wallet no Jaxx (SJuf) [Balanco USD ~11]
