+++
title = "How to generate random names like docker"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-06-15T18:20:11-03:00
tags = ["random", "names"]
categories = ["arch", "linux"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Setup](#setup)
- [How to generate a random names](#how-to-generate-a-random-names)
- [How to generate 10 random names](#how-to-generate-10-random-names)
- [How to generate random names from within Emacs](#how-to-generate-random-names-from-within-emacs)

</div>
<!--endtoc-->

I came up with this awesome tool to generate random names like docker. To install it use:


## Setup {#setup}

```sh
curl -sSf https://fnichol.github.io/names/install.sh | sh
```

This will install the `names` binary in `~/bin`


## How to generate a random names {#how-to-generate-a-random-names}

```sh
names
```


## How to generate 10 random names {#how-to-generate-10-random-names}

```sh
names 10
```


## How to generate random names from within Emacs {#how-to-generate-random-names-from-within-emacs}

To execute within doom emacs use `M-! names` or `SPC o t` to manually open a terminal and use `names`
