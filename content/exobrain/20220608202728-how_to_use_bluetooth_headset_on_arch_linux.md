+++
title = "How to use bluetooth headset on Arch Linux"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-06-08T21:03:32-03:00
tags = ["bluetooth"]
categories = ["arch", "linux"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Install packages](#install-packages)
- [Enable experimental flag](#enable-experimental-flag)
- [Verify that pipewire is working](#verify-that-pipewire-is-working)
- [Find out the device](#find-out-the-device)
- [Connect device](#connect-device)

</div>
<!--endtoc-->



## Install packages {#install-packages}

```sh
sudo pacman -S pipewire-pulse
```


## Enable experimental flag {#enable-experimental-flag}

```sh
sudo vim /etc/bluetooth/main.conf
```

`/Experimental =`

Change it to true

restart the system


## Verify that pipewire is working {#verify-that-pipewire-is-working}

```sh
pactl info | ag -Ui "server name"
```

```text
Server Name: PulseAudio (on PipeWire 0.3.51)
```


## Find out the device {#find-out-the-device}

```sh
sudo systemctl restart bluetooth
```

```text
$ *bluetoothctl*
[CHG] Controller <BSSID> Pairable: yes
[bluetooth]# *list*
Controller <BSSID> <host.name> [default]

[bluetooth]# *power on*
[CHG] Controller <BSSID> Class: 0x006c010c
Changing power on succeeded
[CHG] Controller <BSSID> Powered: yes
[bluetooth]# *agent on*
Agent is already registered
[bluetooth]# *default-agent*
Default agent request successful
[bluetooth]# *scan on*

... a bunch of devices ...
[NEW] Device <dev BSSID> <your device>
```


## Connect device {#connect-device}

```text
[bluetooth]# pair <dev BSSID>
[bluetooth]# trust <dev BSSID>
[bluetooth]# scan off
[bluetooth]# connect <dev BSSID>
```
