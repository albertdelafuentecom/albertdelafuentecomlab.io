+++
title = "Curso de hebreo de EMC Shalom Colombia"
lastmod = 2022-02-02T13:13:15-03:00
tags = ["hebrew", "español", "emacs", "org-mode"]
categories = ["hebrew"]
draft = false
+++

La version en PDF se encuentra <a href="/ox-hugo/20220128220728-curso_de_hebreo_de_emc_shalom_colombia.pdf">aqui</a>.
O [aqui]({{< relref "20220128220728-curso_de_hebreo_de_emc_shalom_colombia" >}}).


## Clase 1 {#clase-1}

| Vocal            | Nombre          | Escrita | biblicalSIL   |
|------------------|-----------------|---------|---------------|
| A                | kamats / qamats | אָ       | `S-. A`       |
| A                | patakh / patah  | אַ       | `S-. a`       |
| A                | khataf kamats   | אֳ       | `S-. Agr-S o` |
| A                | khataf patakh   | אֲ       | `S-. Agr-S a` |
| E                | tseyrey / tsere | אֵ       | `S-. E`       |
| E                | segol           | אֶ       | `S-. e`       |
| E                | khataf segol    | אֱ       | `S-. Agr-S e` |
| E / Silent       | shva / sheva    | אְ       | `S-. ;`       |
| I                | khirik / hiriq  | אִ       | `S-. i`       |
| I (y)            | yod             | י       | `y`           |
| O                | kholam / holam  | אֹ       | `S-. o`       |
| O (no vo)        | khataf kamats   | וֹ       | `S-. Agr-S o` |
| U (no vu)[^fn:1] | shuruk          | ?       | `S-. ?`       |
| U (vu)           | kubuts          | אֻ       | `S-. u`       |

| Letra         | Sonido         | Escrita | biblicalSIL |
|---------------|----------------|---------|-------------|
| 1 - Alef      | -              | אָ       | `S-.`       |
| 2 - Bet / Vet | B / V          | בּ / ב   | `b-=` / `b` |
| 3 - Gimel     | G (gato)       | ג       | `g`         |
| 4 - Daled     | D              | ד       | `d`         |
| 5 - Hey       | H (silenciosa) | ה       | `h`         |
| 6 - Vav       | V              | ו       | `w`         |

-   אַתַה / atah: pronombre tu
-   עַתַה / aatah: ahora
-   moreh / morah: maestro / maestra

-   רוֹאֶה / roeh: ver
-   רוֹעֶה / roeh: pastor
-   פְתַה / ptaj (o petaj)
-   שְׁמַע / shma (o shema)
-   ג׳ / gimmel - apostrofe: sonido de y en español
-   No existe sonido de "y" en español en Hebreo
-   שַׁבַּת / Shabat: Descanso
-   שַׁבַת / Shavat: Hacer huelga o dejar de hacer (del verbo lishvot)
-   שַׁלוֹם / Shalom: Paz / plenitud
-   שַׁלוֹן / Shalon: Hueco
-   Gu(e)vurah: Fuerza o poder en femenino
-   G(e)veret: Señora (viene de Guevura)
-   Guibor: Fuerza en masculino
-   דַוִד / David: Amado (Amado de Elohim)
-   דוֹד / Dod: Amado (Ani le dodi)
-   אַבְרָהָם | Avraam (no Avraham). La h en hebreo no tiene sonido
-   וֹ / o: No vo, para escribir vo necesita una vav delante
-   ווֹ / vo: No existe fonetica de "vo" en una unica letra
-   וֻ / vu: No existe fonetica de "vu" con el punto en el medio, unicamente con 3 puntos. Con el punto en el medio es "u", y no "vu"
-   Plana de vav: va, ve, vi, o, u

Tarea:

-   Escribir la fonética de las vocales a, e, i, o, u para todas
    las variaciones y pronunciando la fonética cada vez que se escribe para crear
    la asociación mental
-   Hacer una hoja de plana por cada letra vs vocal y pronunciando. Para letras
    con daguesh, usar una linea sin daguesh y otra linea con daguesh


## Clase 2 {#clase-2}

| Letra | Sonido        | Escrita     | biblicalSIL               |
|-------|---------------|-------------|---------------------------|
| Zayin | Z             | ז           | `z`                       |
| Jet   | J gutural     | ח           | `x`                       |
| Tet   | T             | ט           | `v`                       |
| Yud   | I             | י           | `y`                       |
| Kaf   | K / J gutural | כּ/ךּ / כ/ך   | `K-=` o `k-=` / `K` o `k` |
| Lamed | L             | ל           | `l`                       |

-   זמָן / zman: Tiempo
-   זְמָן הַזְה / zman hazeh: este tiempo
-   לְחַיִים / lejaim: por las vidas (jaim: vidas, no existe en singular)
-   טוֹב / tov: buen
-   ברוך / baruj: bendito (la ultima es kaf suffit)
-   יבַרֵך / ievarej:
-   עֵינַיִם / e(i)naim: ojos. La primera yud no se pronuncia
-   ג׳ / y'xxx: Sonido de y en español como por ejemplo York. Se usa gimel junto con un apostrofe (Altgr+/).
-   דִגֵ׳יִ / "diyei" (DJ)
-   אַנגֵ׳ליס / angelis
-   בַּרוֻך / baruj: bendito&nbsp;[^fn:2]
-   בֵּרֵך / berej: rodilla (raiz de baruj)
-   חיִ / jai : verbo imperativo vive. Am Israel jai: el pueblo de Israel vive
-   אֵלִיַהוֻ / Eliaju&nbsp;[^fn:2]

Tarea:

-   Escribir cada letra de esta clase (con sus variaciones) con cada vocal
    pronunciando la fonética cada vez que se escribe para crear la asociación
    mental


## Clase 3 {#clase-3}

| Letra | Sonido | Escrita     | biblicalSIL               |
|-------|--------|-------------|---------------------------|
| Mem   | M      | ם/מ         | `m` / `M`                 |
| Nun   | N      | ן/נ         | `n` / `N`                 |
| Samej | S      | ס           | `n`                       |
| Ayin  | -      | ע           | `S-,`                     |
| Pei   | P / F  | פּ/ףּ / פ/ף   | `P-=` o `p-=` / `P` o `p` |
| Tzade | Tz     | צ/ץ         | `c` / `C`                 |
| Kuf   | K      | ק           | `q`                       |

-   **REGLA**: Cuando la jet va al final la vocal siempre va a ser a, y va a ser antes de la consonante y no despues. Ejemplos:
    -   מָשׁיחַ / Mashiaj. Importante no se lee Mashija, siempre se lee la vocal antes que la jet
    -   רוחַ / Ruaj. Viento o espiritu. No Ruja
    -   רוחוֹת / Rujot (rwxwot): Vientos o espiritus. Plural femenino de Ruaj
    -   תַפוחַ / Tapuaj (tApwxa): Manzana (escrito por mi, puede estar mal escrito)
    -   נוֹחַ / Noaj (nwoxa): Noe
-   Im: plural masculino (ajim: hermanos)
-   Ot: plural femenino (ajot: hermanas)
-   Yud es pequeña y para arriba
-   Vav es todo el espacio de la altura de las letras
-   Nun sufit es mayor y baja mas que la altura estandar de las letras
-   סוֻס / sus: caballo&nbsp;[^fn:2]
-   El hebreo en Yemen parece que es mejor pronunciado que incluso en Israel
-   עֵדֵן / Eden: paraiso o delicia, de ahi viene el nombre Edna
-   אָדָם / Adam: humanidad, hecho de Elohim o hecho de sangre
-   **REGLA**: Cuando la pei va despues de cualquier otra letra (como en lefaneja, con lamed antes), la pei pierde el daguesh y se convierte en f, por eso lefaneja y no lepaneja. Lo mismo pasa con la bet que se convierte en vet cuando tiene otra letra delante. Todos los daguesh tienen esa misma regla. Ejemplos:
    -   פַּנךּ / paneja: rostro
    -   לפַנךּ / lefaneja.
    -   בֵּן / ben: hijo
    -   וֻבֵנ / uven&nbsp;[^fn:2]
-   **REGLA**: En hebreo no existen las comas. Las comas se substituyen por la conjuncion. Ejemplo:
    -   Abraham, Isaac y Jacob
    -   Avraham ve Itzaq ve Iaqob
-   יוֹסֵף / Iosef: Jose
-   Eretz: territorio, perteneciente a alguien. Pais, territorio, estado, ciudad. Demarcacion
-   Adama: tierra de sembrar (palabra en femenino de Adam)
-   Zar: principe, zarah seria el femenino de zar, princesa
-   קַדוֹשׁ / Kadosh
-   קֵהִלַה / Kehilah
-   קוֹל / Kol: voz
-   כֹּל / Kol: todo


## Clase 4 {#clase-4}

[^fn:1]: : Vav con punto en el medio del lado izquierdo. No lo consigo en el biblicalSIL. Se pronuncia "U", no "VU".
[^fn:2]: : No se hacer la vav con el punto izquierdo en el medio (u), entonces use los 3 puntos abajo, no se si esta bien escrito.
