+++
title = "How to flash an Android phone"
author = ["Albert De La Fuente Vigliotti"]
date = 2021-07-11
lastmod = 2022-07-21T12:58:18-03:00
tags = ["device", "flash", "android"]
categories = ["device", "flash", "android"]
draft = false
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Android flashing](#android-flashing)
    - [Requirements and assumptions](#requirements-and-assumptions)
    - [Flash a new rom](#flash-a-new-rom)
    - [Manually flash both files](#manually-flash-both-files)
    - [Enable ADB on device](#enable-adb-on-device)
    - [Install the automatic updates "privileged extension" for F-Droid](#install-the-automatic-updates-privileged-extension-for-f-droid)
    - [Rsync](#rsync)
    - [Base configs](#base-configs)
    - [Base features](#base-features)
    - [Computer apps](#computer-apps)
    - [Backup with adb backup (deprecated in the future)](#backup-with-adb-backup--deprecated-in-the-future)
    - [Backup manually selected apps copied via adb within TWRP](#backup-manually-selected-apps-copied-via-adb-within-twrp)
    - [Restore manually selected apps copied via adb within TWRP](#restore-manually-selected-apps-copied-via-adb-within-twrp)
    - [Backup using tetherback](#backup-using-tetherback)
    - [Restore using tetherback](#restore-using-tetherback)
    - [Restore using osmandx](#restore-using-osmandx)
    - [<span class="org-todo todo TODO">TODO</span> Restore](#restore)
    - [Migration](#migration)
    - [In case of lockout of recovery boot mode](#in-case-of-lockout-of-recovery-boot-mode)

</div>
<!--endtoc-->



## Android flashing {#android-flashing}


### Requirements and assumptions {#requirements-and-assumptions}

Adapted from: <https://wiki.lineageos.org/devices/d802/upgrade>

-   The phone has already TWRP as bootloader
-   Latest TWRP is already installed
-   ROM in zip file (latest)
-   The memory is already wiped


### Flash a new rom {#flash-a-new-rom}

```sh
  # Paths. EDIT THIS
  FILES_PATH=/mnt/afuse/nas/share/os/android/lg-g2

  # Reboot in recovery
  adb reboot recovery
  sleep 60

  # Flash files
  adb push $FILES_PATH/lineage-16.0-*.zip /sdcard/lineage16.zip
  adb push $FILES_PATH/addonsu-16.0-arm64-*.zip /sdcard/addonsu16.zip
  adb push contacts.vcf /sdcard/contacts.vcf
```


### Manually flash both files {#manually-flash-both-files}

-   DONT allow "automatically" install anything


### Enable ADB on device {#enable-adb-on-device}

-   Security &amp; location / Encryption &amp; credentials / `encrypt phone`
-   About phone / Build number: `Hit 7 times` the build number
-   System / Developer options /
    -   Debugging / Android Debugging (`ADB`): enabled
    -   `root access`: Apps and ADB
-   `USB preferences`: File Transfer
-   Plug the USB cable


### Install the automatic updates "privileged extension" for F-Droid {#install-the-automatic-updates-privileged-extension-for-f-droid}

-   Reboot in TWRP and push the zip file and flash it
-   Remember to keep unselected all the checkboxes at the end (don't install the APK!)

<!--listend-->

```sh
  adb push $HOME/dropsync/appdata/android/baseapps/org.fdroid.fdroid.privileged.ota_2110.zip /sdcard/
```


### Rsync {#rsync}

```sh
# Source: https://ptspts.blogspot.com/2015/03/how-to-use-rsync-over-adb-on-android.html
wget -O rsync.bin http://github.com/pts/rsyncbin/raw/master/rsync.rsync4android
adb push rsync.bin /data/local/tmp/rsync
adb shell chmod 755 /data/local/tmp/rsync
adb shell cp /data/local/tmp/rsync /sdcard/rsync.bin
adb shell /data/local/tmp/rsync --version
adb shell /system/xbin/rsync --version
```


### Base configs {#base-configs}

```sh
  # Disable captive portal ("Connected, no internet")
  adb shell settings put global captive_portal_detection_enabled 0
  adb shell settings put global captive_portal_mode 0
  adb shell settings put global captive_portal_server example.com
  adb shell settings put global captive_portal_https_server "https://example.com"
  adb shell settings put global captive_portal_http_server "http://example.com"

  # Disable watchdog
  adb shell settings put global wifi_watchdog_on 0
  adb shell settings put global wifi_watchdog_background_check_enabled 0

  # Checks: Disable captive portal ("Connected, no internet")
  adb shell settings get global captive_portal_detection_enabled
  adb shell settings get global captive_portal_mode
  adb shell settings get global captive_portal_server
  adb shell settings get global captive_portal_https_server
  adb shell settings get global captive_portal_http_server

  # Checks: Disable watchdog
  adb shell settings get global wifi_watchdog_on
  adb shell settings get global wifi_watchdog_background_check_enabled
```


### Base features {#base-features}

Encryption

-   Enable it manually. Use at least 6 digits pin passwords
-   Set auto destruction on X failed attempts

Data sync with Nextcould (orgmode, bbdb)

-   Client: <https://f-droid.org/en/packages/com.nextcloud.client/>

Data sync with EteSync: Based on django sync for contacts, calendar and tasks (orgmode, bbdb?)

-   Client: <https://f-droid.org/en/packages/com.etesync.syncadapter/>
-   Server: <https://github.com/etesync/server>

Org mode

-   <https://f-droid.org/en/packages/com.coste.syncorg/>

Access phone over desktop

-   scrcpy

Phone tracking

-   <https://f-droid.org/en/packages/net.eneiluj.nextcloud.phonetrack/>


### Computer apps {#computer-apps}

pacaur -S scrcpy


### Backup with adb backup (deprecated in the future) {#backup-with-adb-backup--deprecated-in-the-future}

See: <https://www.reddit.com/r/LineageOS/comments/b7i1zx/twrp_vs_adb_backup_all_vs_oandbackup/>
See: <https://play.google.com/store/apps/details?id=balti.migrate&hl=en_US>

```sh
  # Variables:
  #PHONE=lg_g2
  PHONE=oneplus_2
  FILES_PATH=/mnt/afuse/nas/share/os/android/$PHONE/backups

  # Reboot in recovery mode with:
  # adb reboot recovery

  adb backup -f $FILES_PATH/`date +'%Y-%m-%d'`.$PHONE.backup.ab --twrp --compress data system boot

  adb reboot system
```


### Backup manually selected apps copied via adb within TWRP {#backup-manually-selected-apps-copied-via-adb-within-twrp}

Also USE

```sh
adb pull /data/data/com.excelliance.multiaccounts.b64
```


### Restore manually selected apps copied via adb within TWRP {#restore-manually-selected-apps-copied-via-adb-within-twrp}

```sh
# FIRST:
# - Install the app manually
# - Open and kill the app
adb root
adb push com.whatsapp /data/data/
```


### Backup using tetherback {#backup-using-tetherback}

-   <https://github.com/dlenski/tetherback>
-   <https://gist.github.com/inhies/5069663>

<!--listend-->

```sh
  pipenv --three
  pipenv install https://github.com/dlenski/tetherback/archive/0.9.1.zip
  pipenv shell

  adb reboot recovery

  # Dont backup to usb flashdrive since throughput will be around 600kbps. Backup to local fs
  # tetherback -M -P -o /home/av/backup
  tetherback -t -M -D -R -C
```


### Restore using tetherback {#restore-using-tetherback}

./adb-sync ../tetherback/twrp-backup-2020-05-08--07-53-02 /sdcard


### Restore using osmandx {#restore-using-osmandx}


### <span class="org-todo todo TODO">TODO</span> Restore {#restore}

See:

```sh
  # Variables:
  #PHONE=lg_g2
  PHONE=oneplus_2
  FILES_PATH=/mnt/afuse/nas/share/os/android/$PHONE/backups

  # Reboot in recovery mode with:
  # adb reboot recovery

  adb restore --twrp

```


### Migration {#migration}

See: <https://forum.xda-developers.com/android/apps-games/app-migrate-custom-rom-migration-tool-t3862763>
See: <https://play.google.com/store/apps/details?id=balti.migrate&hl=en_US>


### In case of lockout of recovery boot mode {#in-case-of-lockout-of-recovery-boot-mode}

```sh
fastboot reboot
fastboot devices
fastboot oem unlock
fastboot flash recovery Downloads/twrp-3.2.1-0-oneplus2.img
```
