+++
title = "Experimento do vinho de jabuticaba"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-11-29
lastmod = 2022-01-11T23:36:48-03:00
tags = ["fermenting", "wine", "jabuticaba"]
categories = ["homestead"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Experimento finalizado no dia 29/11/2020](#experimento-finalizado-no-dia-29-11-2020)
    - [Conclusoes e melhoras](#conclusoes-e-melhoras)

</div>
<!--endtoc-->



## Experimento finalizado no dia 29/11/2020 {#experimento-finalizado-no-dia-29-11-2020}

-   Colhi um balde preto de lavar a roupa de jabuticabas
-   Coloquei numa garrafa de 5 litros de agua
-   Se nao me engano acrescentei 1 chicara de acucar
    (talvez foi muito, o vinho parece ter um teor alcolico notavel)
-   Fermentei por 3 semanas exatas
-   Filtrei usando filtro 102 de cafe da rancheiro e a filtragem foi rapida
-   Numa garrafa eu acrescentei 5 colheres de sopa de acucar cristal
    (aquela que nao endoca muito do Floresta)
-   Na outra eu acrescentei 12 colheres de sopa de acucar cristal
    (acho que e de mais)


### Conclusoes e melhoras {#conclusoes-e-melhoras}

-   As jabuticabas devem ser amassadas antes pois muitas estava meio que com ar
    dentro porem sem ter estourado, talvez algum tipo de efeito de osmose
    aconteceu devido ao acucar.
-   O vinho ficou um pouco amargo para o meu gosto, talvez vale a pena fermentar
    por menos tempo, uns 10 dias e depois tirar so o liquido e deixar fermentar
    sozinho mais tempo
-   A engenhoca para fermentacao anaerobica que fiz funcionou ate que bem
-   Talvez vale a pena separar um pouco de casca e destinar a geleia, ou seja do
    volume total, talvez 30% com casca e o resto so o miolo e destinar o resto da
    casca para geleia e assim ter 2 produtos ja que houve muito desperdiço
-   Se ficar igual de seco/amargo, talvez umas 5 a 7 colheres de acucar antes de
    filtrar seja uma boa medida para tornar o vinho mais de mesa
