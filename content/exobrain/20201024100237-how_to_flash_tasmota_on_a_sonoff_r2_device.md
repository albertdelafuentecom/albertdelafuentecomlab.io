+++
title = "How to flash Tasmota on a Sonoff R2 device"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-10-24
lastmod = 2022-01-11T21:13:31-03:00
tags = ["device", "flash", "sonoff", "tasmota"]
categories = ["device", "flash"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [How to flash Tasmota on a Sonoff R2 Device](#how-to-flash-tasmota-on-a-sonoff-r2-device)
    - [Upgrade firmware to the latest version](#upgrade-firmware-to-the-latest-version)
    - [Cable pin connections (male) on the Sonoff device](#cable-pin-connections--male--on-the-sonoff-device)
    - [Cable pin connections (female) on the FTDI-USB device](#cable-pin-connections--female--on-the-ftdi-usb-device)
    - [Step 1: Ensure the connection is working properly](#step-1-ensure-the-connection-is-working-properly)
    - [Step 2: Backup the current firmware image](#step-2-backup-the-current-firmware-image)
    - [Step 3: Erase the flash memory](#step-3-erase-the-flash-memory)
    - [Step 4: Upload the Tasmota firmware](#step-4-upload-the-tasmota-firmware)
    - [Step 5: Configure the wireless access](#step-5-configure-the-wireless-access)
    - [Resources](#resources)
        - [Flashing Tasmota on Sonoff Basic R2 using Linux - One Guy, One Blog](#flashing-tasmota-on-sonoff-basic-r2-using-linux-one-guy-one-blog)
        - [Getting MicroPython on a Sonoff Smart Switch | by Cloud4RPi Team | Cloud4RPi | Medium](#getting-micropython-on-a-sonoff-smart-switch-by-cloud4rpi-team-cloud4rpi-medium)

</div>
<!--endtoc-->



## How to flash Tasmota on a Sonoff R2 Device {#how-to-flash-tasmota-on-a-sonoff-r2-device}


### Upgrade firmware to the latest version {#upgrade-firmware-to-the-latest-version}

-   Install the Ewelink app
-   Update the firmware to the latest version


### Cable pin connections (male) on the Sonoff device {#cable-pin-connections--male--on-the-sonoff-device}

On the back connect the cables as:

black - gnd
white - tx
gray - rx
purple - vcc


### Cable pin connections (female) on the FTDI-USB device {#cable-pin-connections--female--on-the-ftdi-usb-device}

On the FTDI device connect the cables as

black - gnd
white - RX (inverted)
gray - TX (inverted)
purple - vcc


### Step 1: Ensure the connection is working properly {#step-1-ensure-the-connection-is-working-properly}

```sh
  pipenv --three
  pipenv install esptool
  pipenv shell
  sudo dmesg | ag -Ui tty
```

Press the button while connecting the USB cable

```sh
  sudo esptool.py --port /dev/ttyUSB0 flash_id
```


### Step 2: Backup the current firmware image {#step-2-backup-the-current-firmware-image}

Press the button while connecting the USB cable

```sh
  sudo esptool.py --port /dev/ttyUSB0 read_flash 0x00000 0x100000 version-3.5.0.bin
```


### Step 3: Erase the flash memory {#step-3-erase-the-flash-memory}

Press the button while connecting the USB cable

```sh
  sudo esptool.py --port /dev/ttyUSB0 erase_flash
```


### Step 4: Upload the Tasmota firmware {#step-4-upload-the-tasmota-firmware}

Press the button while connecting the USB cable

```sh
  sudo esptool.py --port /dev/ttyUSB0 write_flash -fs 1MB -fm dout 0x0 tasmota.bin
```


### Step 5: Configure the wireless access {#step-5-configure-the-wireless-access}

-   Restart the device
-   Connect to the "tasmota-X" network AP
-   Browse to 192.168.4.1
-   Scan networks and type password


### Resources {#resources}


#### [Flashing Tasmota on Sonoff Basic R2 using Linux - One Guy, One Blog](https://oneguyoneblog.com/2020/02/29/flashing-tasmota-on-sonoff-basic-r2-using-linux/) {#flashing-tasmota-on-sonoff-basic-r2-using-linux-one-guy-one-blog}

-   Source: <https://oneguyoneblog.com/2020/02/29/flashing-tasmota-on-sonoff-basic-r2-using-linux/>
-   Title: Flashing Tasmota on Sonoff Basic R2 using Linux - One Guy, One Blog
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2020-10-03 Sat]</span></span>


#### [Getting MicroPython on a Sonoff Smart Switch | by Cloud4RPi Team | Cloud4RPi | Medium](https://medium.com/cloud4rpi/getting-micropython-on-a-sonoff-smart-switch-1df6c071720a) {#getting-micropython-on-a-sonoff-smart-switch-by-cloud4rpi-team-cloud4rpi-medium}

-   Source: <https://medium.com/cloud4rpi/getting-micropython-on-a-sonoff-smart-switch-1df6c071720a>
-   Title: Getting MicroPython on a Sonoff Smart Switch | by Cloud4RPi Team | Cloud4RPi | Medium
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2020-10-25 Sun]</span></span>
