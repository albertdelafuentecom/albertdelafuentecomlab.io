+++
title = "How to renew a GPG key"
author = ["Albert De La Fuente Vigliotti"]
date = 2020-11-22
lastmod = 2022-01-11T20:48:53-03:00
tags = ["crypto", "gpg"]
categories = ["crypto", "gpg"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [How to renew a GPG key](#how-to-renew-a-gpg-key)
    - [How to renew the main key](#how-to-renew-the-main-key)
    - [How to renew the subkey](#how-to-renew-the-subkey)
    - [Renew the main key in an unattended way](#renew-the-main-key-in-an-unattended-way)
    - [<span class="org-todo todo TODO">TODO</span> NEEDS TESTING: Renew the subkey in an unattended way](#needs-testing-renew-the-subkey-in-an-unattended-way)
    - [Read more](#read-more)
        - [gnupg - Extend the expiration date of a GPG key non-interactively - Super User](#gnupg-extend-the-expiration-date-of-a-gpg-key-non-interactively-super-user)
        - [<span class="org-todo todo TODO">TODO</span> Create new stronger key with a transition statement according to: OpenPGP Best Practices - riseup.net](#create-new-stronger-key-with-a-transition-statement-according-to-openpgp-best-practices-riseup-dot-net)

</div>
<!--endtoc-->



## How to renew a GPG key {#how-to-renew-a-gpg-key}


### How to renew the main key {#how-to-renew-the-main-key}

```text
  gpg2 --list-secret-keys
  gpg2 --edit-key 0x536814BF4871A220
  gpg> expire
  10w
  gpg> save
```


### How to renew the subkey {#how-to-renew-the-subkey}

Since I have subkeys I need to also renew the subkey as follows:

```text
  gpg2 --list-secret-keys
  gpg2 --edit-key 0x536814BF4871A220
  gpg> key 0xE2977BF3F82AB971
  gpg> expire
  10w
  gpg> save
```


### Renew the main key in an unattended way {#renew-the-main-key-in-an-unattended-way}

The automated way to renew the main key is as follows

```bash
  printf "expire\n10w\nsave\n" | gpg --batch --pinentry-mode loopback \
  --passphrase-fd 3 --command-fd 0 --status-fd=2 \
  --edit-key vonpupp@keybase.io 3<passphrase.txt
  rm passphrase.txt
```


### <span class="org-todo todo TODO">TODO</span> NEEDS TESTING: Renew the subkey in an unattended way {#needs-testing-renew-the-subkey-in-an-unattended-way}

```bash
  printf "key 0xE2977BF3F82AB971\nexpire\n10w\nsave\n" | gpg --batch --pinentry-mode loopback \
  --passphrase-fd 3 --command-fd 0 --status-fd=2 \
  --edit-key vonpupp@keybase.io 3<passphrase.txt
  rm passphrase.txt
```


### Read more {#read-more}


#### [gnupg - Extend the expiration date of a GPG key non-interactively - Super User](https://superuser.com/questions/1478615/extend-the-expiration-date-of-a-gpg-key-non-interactively) {#gnupg-extend-the-expiration-date-of-a-gpg-key-non-interactively-super-user}

-   Source: <https://superuser.com/questions/1478615/extend-the-expiration-date-of-a-gpg-key-non-interactively>
-   Title: gnupg - Extend the expiration date of a GPG key non-interactively - Super User
-   Captured on: <span class="timestamp-wrapper"><span class="timestamp">[2020-11-22 Sun]</span></span>


#### <span class="org-todo todo TODO">TODO</span> Create new stronger key with a transition statement according to: [OpenPGP Best Practices - riseup.net](https://riseup.net/ru/security/message-security/openpgp/gpg-best-practices#key-configuration) {#create-new-stronger-key-with-a-transition-statement-according-to-openpgp-best-practices-riseup-dot-net}

-   Source: <https://riseup.net/ru/security/message-security/openpgp/gpg-best-practices#key-configuration>
-   Title: OpenPGP Best Practices - riseup.net

> It is recommend to make a 3072-bit RSA key, with the sha512 hashing algo, making a transition statement that is signed by both keys, and then letting people know. Also have a look at this good document that details exactly the steps that you need to create such a key, making sure that you are getting the right hashing algo (it can be slightly complicated if you are using GnuPG versions less than 1.4.10).
