+++
title = "How to configure the keyboard layout for Hebrew (biblicalSIL, phonetic, etc)"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-02-01T23:23:40-03:00
tags = ["hebrew"]
categories = ["hebrew"]
draft = false
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [How to configure the keyboard layout for Hebrew](#how-to-configure-the-keyboard-layout-for-hebrew)
    - [How to use multiple layouts and variants on setxkbmap](#how-to-use-multiple-layouts-and-variants-on-setxkbmap)
        - [Biblical SIL keyboard layout](#biblical-sil-keyboard-layout)
        - [Biblical keyboard layout](#biblical-keyboard-layout)
        - [How to list the available layout variants](#how-to-list-the-available-layout-variants)
        - [Other layouts that might be worthwhile to look at](#other-layouts-that-might-be-worthwhile-to-look-at)
    - [Hebrew-mode (worthwhile checking it out)](#hebrew-mode--worthwhile-checking-it-out)
    - [Anki-vocaulary and Youdao](#anki-vocaulary-and-youdao)
    - [How to change Doom Emacs font on the fly (not working)](#how-to-change-doom-emacs-font-on-the-fly--not-working)
    - [How to use keyman + kimtoy](#how-to-use-keyman-plus-kimtoy)
    - [Biblical Hebrew Fonts](#biblical-hebrew-fonts)

</div>
<!--endtoc-->



## How to configure the keyboard layout for Hebrew {#how-to-configure-the-keyboard-layout-for-hebrew}


### How to use multiple layouts and variants on setxkbmap {#how-to-use-multiple-layouts-and-variants-on-setxkbmap}

```shell
setxkbmap -layout "eu,il,il" -variant ",biblicalSIL,biblical" -option -option "lv3:ralt_switch,grp:rctrl_rshift_toggle"
```

To cycle the layout hold `right shift + right control`. All available keystrokes
combination can be found here&nbsp;[^fn:1]. Note that the first `-option` is to reset
any other old option in use.

The available layouts are located in `/usr/share/X11/xkb/rules`. You can grep
for SIL for example.


#### Biblical SIL keyboard layout {#biblical-sil-keyboard-layout}

Since my intention is to learn to read biblical hebrew (and write it), I will
settle up for `biblicalSIL` for the time being. It seems to be a good starting
point, since it has Niqqud and a pretty logical layout.

The full documentation of the layout can be found here&nbsp;[^fn:2]


#### Biblical keyboard layout {#biblical-keyboard-layout}

It seems to be the standard hebrew keyboard layout. Probably it will also match
the android hebrew keyboard, if so it is good to learn it. I am not sure if it
does support niqqud vowel pointing.

The full documentation of the layout can be found here&nbsp;[^fn:3].


#### How to list the available layout variants {#how-to-list-the-available-layout-variants}

```shell
  localectl list-x11-keymap-variants il
```

The output should be something similar to this:

```text
biblical
lyx
phonetic
```


#### Other layouts that might be worthwhile to look at {#other-layouts-that-might-be-worthwhile-to-look-at}

<https://github.com/kyriesent/type-biblical-languages-linux-xkb>

<https://frame-poythress.org/keyboard-entry-of-polytonic-greek-and-biblical-hebrew-in-gnulinux-2014/>


### Hebrew-mode (worthwhile checking it out) {#hebrew-mode--worthwhile-checking-it-out}

<https://github.com/ymarco/hebrew-mode>


### Anki-vocaulary and Youdao {#anki-vocaulary-and-youdao}

<https://github.com/lujun9972/anki-vocabulary.el>


### How to change Doom Emacs font on the fly (not working) {#how-to-change-doom-emacs-font-on-the-fly--not-working}

```elisp
(setq doom-font (font-spec :family "Input Mono Narrow" :size 12 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "Fira Sans") ; inherits `doom-font''s :size
      doom-unicode-font (font-spec :family "Input Mono Narrow" :size 12)
      doom-big-font (font-spec :family "Fira Mono" :size 19))
```

```elisp
(setq doom-unicode-font (font-spec :family "Cardo" :size 12))
```

ַ.אאאאעעעעאאא

```text
#<font-spec nil nil Cardo nil nil nil nil nil 12 nil nil nil nil>
```


### How to use keyman + kimtoy {#how-to-use-keyman-plus-kimtoy}

See: <https://help.keyman.com/knowledge-base/102>

I could not make it to work, and it doesn't seem to be the preferred method anyway.


### Biblical Hebrew Fonts {#biblical-hebrew-fonts}

<https://software.sil.org/ezra/>

Perhaps Cardo is preferred, since it has greek also. I am not sure, more testing is needed.

[^fn:1]: : <https://unix.stackexchange.com/questions/45447/other-than-alt-shift-to-switch-keyboard-layout-any-other-xorg-key-combination>
[^fn:2]: : <https://www.sbl-site.org/Fonts/BiblicalHebrewSILManual.pdf>
[^fn:3]: : <https://en.wikipedia.org/wiki/Hebrew%5Fkeyboard>
