+++
title = "How to list all projects in Github"
author = ["Albert De La Fuente Vigliotti"]
lastmod = 2022-07-03T12:24:06-03:00
tags = ["github", "cli"]
categories = ["arch", "linux"]
draft = false
+++

Related notes
: [How to list all projects in Gitlab]({{< relref "20220703004228-how_to_list_all_projects_in_gitlab.md" >}}), [How to list all projects in BitBucket]({{< relref "20220703112132-how_to_list_all_projects_in_bitbucket.md" >}})

Install the github-cli with:

```sh
pacaur -S github-cli
```

Auth with:

```sh
gh auth login
```

Once configured, the magic line to list all the repos by LRU is:

```sh
gh repo list -L 3000 --json nameWithOwner --jq .[].nameWithOwner
```
